
update servicos.menu set page_id = null;
delete from servicos.page where 1=1;
alter table servicos.page AUTO_INCREMENT = 0;

INSERT INTO servicos.page (crud, title, url) 
values
('R', 'index', '\\index.html'),
('R', 'dash', '\\dash.html'),
-- ('R', 'os-dialog', '\\dialogs\\os-dialog.html'),
-- ('R', 'footer', '\\footer\\footer.html'),
('R', 'login', '\\login.html'),
-- ('R', 'nav', '\\nav\\nav.html'),

('R', 'reset-password', '\\reset-password.html'),
('R', 'forgot-password', '\\forgot-password.html'),

('CRUD', 'role-detail', '\\role\\role-detail.html'),
('CRUD', 'role', '\\role\\role.html'),

('CRUD', 'user-profile', '\\user-profile\\detail\\user-profile.html'),
('CRUD', 'user-detail', '\\user\\user-detail.html'),
('CRUD', 'user', '\\user\\user.html'),


('CRUD', 'atividade-economica-detail', '\\atividade-economica\\atividade-economica-detail.html'),
('CRUD', 'atividade-economica', '\\atividade-economica\\atividade-economica.html'),


('CRUD', 'colaborador-cadastrar', '\\colaborador\\colaborador-cadastrar.html'),
('CRUD', 'colaborador-detail', '\\colaborador\\colaborador-detail.html'),
('CRUD', 'colaborador', '\\colaborador\\colaborador.html'),

('CRUD', 'empresa-detail', '\\empresa\\empresa-detail.html'),
('CRUD', 'empresa', '\\empresa\\empresa.html'),

('CRUD', 'filial-detail', '\\filial\\filial-detail.html'),
('CRUD', 'filial', '\\filial\\filial.html'),

('CRUD', 'fornecedor-cadastrar', '\\fornecedor\\fornecedor-cadastrar.html'),
('CRUD', 'fornecedor-detail', '\\fornecedor\\fornecedor-detail.html'),
('CRUD', 'fornecedor', '\\fornecedor\\fornecedor.html'),

('CRUD', 'marca-veiculo-detail', '\\marca-veiculo\\marca-veiculo-detail.html'),
('CRUD', 'marca-veiculo', '\\marca-veiculo\\marca-veiculo.html'),

('CRUD', 'situacao-servico-detail', '\\situacao-servico\\situacao-servico-detail.html'),
('CRUD', 'situacao-servico', '\\situacao-servico\\situacao-servico.html'),

('CRUD', 'natureza-servico-detail', '\\natureza-servico\\natureza-servico-detail.html'),
('CRUD', 'natureza-servico', '\\natureza-servico\\natureza-servico.html'),

('CRUD', 'ordem-servico-detail', '\\ordem-servico\\ordem-servico-detail.html'),
('CRUD', 'ordem-servico', '\\ordem-servico\\ordem-servico.html'),
('R', 'ordem-servico-cadastrar', '\\ordem-servico\\ordem-servico.html?{mode:insert}'),
	
('CRUD', 'parametro-detail', '\\parametro\\parametro-detail.html'),
('CRUD', 'parametro', '\\parametro\\parametro.html'),

('CRUD', 'municipio-detail', '\\municipio\\municipio-detail.html'),
('CRUD', 'municipio', '\\municipio\\municipio.html'),

('CRUD', 'estado-detail', '\\estado\\estado-detail.html'),
('CRUD', 'estado', '\\estado\\estado.html'),

('CRUD', 'regiao-detail', '\\regiao\\regiao-detail.html'),
('CRUD', 'regiao', '\\regiao\\regiao.html'),

('R', 'veiculo-alterar-situacao', '\\veiculo\\veiculo-alterar-situacao.html'),
('R', 'veiculo-cadastrar', '\\veiculo\\veiculo-cadastrar.html?{mode:insert}'),
('CRUD', 'veiculo-detail', '\\veiculo\\veiculo-detail.html'),
('CRUD', 'veiculo', '\\veiculo\\veiculo.html'),

('R', 'report-veiculo-listagem', '\\report\\report-veiculo-listagem.html'),
('R', 'role-select-dialog', '\\dialogs\\role-select-dialog.html'),
('CRUD', 'user-select-dialog', '\\user-select-dialog.html'),
('CRUD', 'regiao-dialog', '\\regiao-dialog.html'),
('CRUD', 'atividade-economica-select-dialog', '\\fornecedor\\atividade-economica-select-dialog.html');

select * from servicos.page;
-- =======================================================================================================

update servicos.menu set parent_menu = null;

delete from servicos.role_menus where 1=1;
delete from servicos.role_menu where 1=1;
delete from servicos.menu where 1=1;

alter table servicos.menu AUTO_INCREMENT = 0;



set @page = 0;
set @page = (select id from page where title = 'index');
INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
values('InicioMenu', 'Menu Inicial', 0, @page, null);
set @home = LAST_INSERT_ID();

set @page = (select id from page where title = 'veiculo');
INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
values('Veiculo', 'Veículos', 1, @page, @home);
-- set @item = LAST_INSERT_ID();
-- 
-- 	set @page = (select id from page where title = 'veiculo-cadastrar');
-- 	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
-- 	values('Incluir Veículo', 1.1, @page, @item);
-- 	
-- 	set @page = (select id from page where title = 'veiculo-alterar-situacao');
-- 	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
-- 	values('Alterar Situação', 1.2, @page, @item);
-- 	
-- 	set @page = (select id from page where title = 'veiculo');
-- 	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
-- 	values('Listagem Veículos', 1.3, @page, @item);

set @page = (select id from page where title = 'ordem-servico');
INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
values('OrdemServico', 'Ordens de Serviço', 2, @page, @home);
set @item = LAST_INSERT_ID();

-- 	set @page = (select id from page where title = 'ordem-servico-cadastrar');
-- 	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
-- 	values('Incluir OS', 2.1, @page, @item);
-- 	
-- 	set @page = (select id from page where title = 'ordem-servico');
-- 	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
-- 	values('Listagem OS', 2.2, @page, @item);
	
INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
values('Cadastros', 'Cadastros', 3, null, @home);
set @item = LAST_INSERT_ID();

	set @page = (select id from page where title = 'fornecedor');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('Fornecedor', 'Fornecedores', 3.1, @page, @item);
	
	set @page = (select id from page where title = 'colaborador');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('Colaborador', 'Colaboradores', 3.2, @page, @item);
  
INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
values('Relatorios', 'Relatórios', 4, null, @home);
set @item = LAST_INSERT_ID();

	set @page = (select id from page where title = 'report-veiculo-listagem');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('RelatorioListagemVeiculo', 'Listagem de Veículos', 4.1, @page, @item);
	  
INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
values('Tabelas', 'Tabelas', 5, null, @home);
set @item = LAST_INSERT_ID();
	
	set @page = (select id from page where title = 'marca-veiculo');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('MarcaVeiculo', 'Marcas de Veículos', 5.1, @page, @item);
	
	set @page = (select id from page where title = 'situacao-servico');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('SituacaoServico', 'Situações de Serviços', 5.2, @page, @item);
	
	set @page = (select id from page where title = 'natureza-servico');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('NaturezaServico', 'Naturezas de Serviços', 5.3, @page, @item);

	set @page = (select id from page where title = 'atividade-economica');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('AtividadeEconomica', 'Atividade Econômica', 5.4, @page, @item);

	set @page = (select id from page where title = 'municipio');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('Municipio', 'Municípios', 5.5, @page, @item);
	
	set @page = (select id from page where title = 'estado');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('Estado', 'Estados', 5.6, @page, @item);

	set @page = (select id from page where title = 'regiao');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('Regiao', 'Regiões', 5.7, @page, @item);
	  
INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
values('Seguranca', 'Segurança', 6, null, @home);
set @item = LAST_INSERT_ID();
	
	set @page = (select id from page where title = 'user');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('Usuario', 'Usuários', 6.1, @page, @item);
	
	set @page = (select id from page where title = 'role');
	INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
	values('PerfilAcesso', 'Perfis de Acesso', 6.2, @page, @item);

-- set @page = (select id from page where title = 'login');
-- INSERT INTO servicos.menu (name, description, `sequence`, page_id, parent_menu)
-- values('Sair', 7, @page, @home);

select * from servicos.menu m;

delete from servicos.role_menus where 1= 1;
delete from servicos.role_menu where 1= 1;
alter table servicos.role_menu AUTO_INCREMENT = 0;

insert into servicos.role_menu (menu_id, cr, rd, up, dl)
select id, 1, 1, 1, 1 from servicos.menu order by id;

select r.id, r.name, rms.role_id, rms.role_menu_id, rm.id, rm.menu_id, m.id, m.name, p.title
from role r 
left join role_menus rms on rms.role_id = r.id
left join role_menu rm on rm.id = rms.role_menu_id
left join menu m on m.id = rm.menu_id
left join page p on p.id = m.page_id;

set @role_menu = (select max(id) from servicos.role_menu);
select @role_menu;

insert into servicos.role_menus (role_id, role_menu_id)
select 1, id from role_menu order by id;

select * from servicos.menu;
select * from servicos.role_menu;
select * from servicos.role_menus;

select * from `user`;
select * from `role`;

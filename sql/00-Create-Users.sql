USE mysql;

CREATE DATABASE servicos;

USE servicos;

CREATE USER 'servicos_admin'@'%' IDENTIFIED BY 'abc@1234';

CREATE USER 'servicos_oper'@'%' IDENTIFIED BY 'abc@1234';

GRANT ALL PRIVILEGES ON servicos.* TO 'servicos_admin'@'%' ;

GRANT SELECT, INSERT, UPDATE, DELETE ON servicos.* TO 'servicos_oper'@'%' ;

FLUSH PRIVILEGES;


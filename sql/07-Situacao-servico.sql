
update servicos.menu set page_id = null where 1=1;
delete from servicos.page where 1=1;
alter table servicos.page AUTO_INCREMENT = 0;




select * from servicos.page;
-- =======================================================================================================

update servicos.menu set parent_menu = null;

-- delete from servicos.menu where 1=1;

alter table servicos.menu AUTO_INCREMENT = 0;

set @page = 0;
set @page = (select id from servicos.page where title = 'index');
INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Home', 0, @page, null);
set @home = LAST_INSERT_ID();

INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Veiculos', 1, @page, @home);
set @item = LAST_INSERT_ID();

	set @page = (select id from servicos.page where title = 'veiculo-cadastrar');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Incluir Veículo', 1.1, @page, @item);
	
	set @page = (select id from servicos.page where title = 'veiculo-alterar-situacao');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Alterar Situação', 1.2, @page, @item);
	
	set @page = (select id from servicos.page where title = 'veiculo');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Listagem Veículos', 1.3, @page, @item);

INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Ordens de Serviço', 2, null, @home);
set @item = LAST_INSERT_ID();

	set @page = (select id from servicos.page where title = 'ordem-servico?{mode:insert}');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Incluir OS', 2.1, @page, @item);
	
	set @page = (select id from servicos.page where title = 'ordem-servico');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Listagem OS', 2.2, @page, @item);
	
	set @page = (select id from servicos.page where title = 'situacao-servico');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Situacao de Serviço', 2.3, @page, @item);
	
	set @page = (select id from servicos.page where title = 'natureza-servico');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Natureza de Serviço', 2.4, @page, @item);

INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Cadastros', 3, null, @home);
set @item = LAST_INSERT_ID();

	set @page = (select id from servicos.page where title = 'fornecedor');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Fornecedores', 3.1, @page, @item);
	
	set @page = (select id from servicos.page where title = 'colaborador');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Colaboradores', 3.2, @page, @item);
  
INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Relatórios', 4, null, @home);
set @item = LAST_INSERT_ID();

	set @page = (select id from servicos.page where title = 'report-listagem-veiculo');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Listagem de Veículos', 4.1, @page, @item);
	  
INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Tabelas', 5, null, @home);
set @item = LAST_INSERT_ID();
	
	set @page = (select id from servicos.page where title = 'marca-veiculo');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Marcas de Veiculos', 5.1, @page, @item);
	
	set @page = (select id from servicos.page where title = 'atividade-economica');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Atividades Econômicas', 5.2, @page, @item);

	set @page = (select id from servicos.page where title = 'municipio');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Municipios', 5.3, @page, @item);
	
	set @page = (select id from servicos.page where title = 'estado');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Estados', 5.4, @page, @item);

	set @page = (select id from servicos.page where title = 'regiao');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Regiões', 5.5, @page, @item);
	  
INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Segurança', 6, null, @home);
set @item = LAST_INSERT_ID();
	
	set @page = (select id from servicos.page where title = 'usuario');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Usuários', 6.1, @page, @item);
	
	set @page = (select id from servicos.page where title = 'role');
	INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
	values('Perfis de Acesso', 6.2, @page, @item);

set @page = (select id from servicos.page where title = 'login');
INSERT INTO servicos.menu (name, `sequence`, page_id, parent_menu)
values('Sair', 7, @page, @home);

select * from servicos.page p;
select * from servicos.menu m;

delete from servicos.role_menus where 1= 1;
delete from servicos.role_menu where 1= 1;
update servicos.menu set parent_menu = null where 1= 1;
delete from servicos.menu where 1= 1;
delete from servicos.page where 1= 1;
alter table servicos.role_menu AUTO_INCREMENT = 0;
alter table servicos.menu AUTO_INCREMENT = 0;

insert into servicos.role_menu (menu_id, crud)
select id, 'CRUD' from servicos.menu order by id;

set @role_menu = (select max(id) from servicos.role_menu);
select @role_menu;

insert into servicos.role_menus (role_id, role_menu_id)
select 1, id from role_menu order by id;

select * from servicos.role_menu;
select * from servicos.role_menus;

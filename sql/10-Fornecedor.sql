delete from servicos.fornecedor where 1=1 ;

alter table servicos.fornecedor auto_increment = 0;

INSERT INTO servicos.fornecedor (apelido_nome_fantasia,ativo,bairro,cep,complemento,cpf_cnpj,email,genero,identidade_inscricao_estadual,logradouro,nascimento_abertura,nome_razao_social,notas,tipo_juridico,uf,municipio_id) 
VALUES 
('FELICIO',1,'CENTRO NORTE','78.110-400','','32.992.158/0001-81','','OUT','FELICIO','Av. João Ponce de Arruda, 330 - Jardim Aeroporto, Várzea Grande - MT','1989-06-15 00:00:00.000','FELICIO AUTO CENTER',NULL,'PJ','MT',5317)
,('ALEMÃO',1,'CENTRO','78.110-368','','26.898.333/0001-73','','OUT','insento','R. JAIME BENEVIDES, 50',NULL,'ALEMAO AUTO CENTER',NULL,'PJ','MT',5317)
,('DAVI',1,'JARDIM AMERICA','78.110-670','','073.661.606-36','','OUT','000775947','R. BOM JESUS, 766','2015-07-15 00:00:00.000','DAVID AUTO CENTER',NULL,'PJ','MT',5317)
,('ALEXANDRE PNEUS',1,'CIDADE VERDE','','11842','031.154.301-40','','OUT','1','AV MIGUEL SUTIL',NULL,'ALEXANDRE PNEUS E RODAS',NULL,'PJ','MT',5220)
,('TRANSFORME',1,'MANGA','78.115-560','','14.002.715/0001-98','','OUT','ISENTO','R IRMA ELVIRA, 501',NULL,'TRANSFORME FUNILARIA E PINTURA',NULL,'PJ','MT',5317)
,('WM',1,'CENTRO','78.110-335','','20.927.360/0001-40','','OUT','ISENTO','R. SANTOS DUMONT, 45',NULL,'WM LAVA CAR',NULL,'PJ','MT',5317)
,('CLAUDIO FUNILARIA',1,'CENTRO','78.110-400','','33.657.008/0001-84','','OUT','00065','AV. COUTO MAGALHÃES, 288',NULL,'KADETT FUNILARIA E PINTURA',NULL,'PJ','MT',5317)
,('SCUDERIA',1,'BANDEIRANTES','78.010-000','','06.175.291/0001-35','','OUT','1','AV GENERAL VALLE, 528',NULL,'SCUDERIA LAVAGENS ESPECIAIS',NULL,'PJ','MT',5317)
,('STUDIO LAVA JATO',1,'CENTRO SUL','78.110-046','','896.112.925-20','','OUT','0773355637','RUA ALMIRANTE BARROSO, 113',NULL,'STUDIO CENTRO DE ESTETICA AUTOMOTIVA',NULL,'PJ','MT',5317)
,('LÉO CAPAS',1,'PONTE NOVA','78.115-000','','14.303.037/0001-01','','OUT','33183304/9','AV. da FEB, 1344',NULL,'LEANDRO DE ALMEIDA SILVA',NULL,'PJ','MT',5317)
;
INSERT INTO servicos.fornecedor (apelido_nome_fantasia,ativo,bairro,cep,complemento,cpf_cnpj,email,genero,identidade_inscricao_estadual,logradouro,nascimento_abertura,nome_razao_social,notas,tipo_juridico,uf,municipio_id) VALUES 
('DAMAZUL',1,'CENTRO','78.110-400','','00.074.215/0001-57','','OUT','131547038','AV. COUTO MAGALHÃES, 295',NULL,'DAMAZUL MECANICA',NULL,'PJ','MT',5317)
;
select * from servicos.fornecedor ;
CREATE USER 'servico_oper'@'%' IDENTIFIED BY PASSWORD '';
GRANT SELECT, INSERT, UPDATE, DELETE ON `servicos`.* TO 'servico_oper'@'%';

CREATE USER 'servico_admin'@'%' IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON `servicos`.* TO 'servico_admin'@'%';


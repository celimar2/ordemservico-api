package br.net.gradual.core.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Scanner;

public class Util {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("#################################################################");
        System.out.println();
        System.out.print  ("type the word..: ");
        String accessKey = scanner.nextLine();
        System.out.println("the word typed..: " + accessKey);
        System.out.println("encrypted.......: "+(new BCryptPasswordEncoder()).encode(accessKey));
        System.out.println();
        System.out.println("#################################################################");
    }
}

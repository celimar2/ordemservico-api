package br.net.gradual.core.service;

import br.net.gradual.business.model.Colaborador;
import br.net.gradual.core.model.Role;
import br.net.gradual.core.model.RoleMenu;
import br.net.gradual.core.model.User;
import br.net.gradual.core.model.UserDto;

import java.util.List;

public interface UserService {

    User save(UserDto user);

    List<User> findAll();

    void delete(long id);

    User findOne(String username);

    User findById(Long id);

    User findByEmail(String email);

    User findByColaborador(Colaborador colaborador);

    List<User> listAllNoColaborador();

    void saveRoles(Long id, List<Role> roles);

    void setLoginAttempts(String username, boolean loginSuccess);

    void setLastLogin(String username);

    void createPasswordResetTokenForUser(User user, String token);

    User validatePasswordResetToken(Long id, String token);

    List<RoleMenu> getMenuAccess(User user);

}

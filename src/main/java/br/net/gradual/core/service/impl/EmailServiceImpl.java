package br.net.gradual.core.service.impl;

import br.net.gradual.core.model.Mail;
import br.net.gradual.core.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Locale;


@Service(value = "emailService")
public class EmailServiceImpl implements EmailService{

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    public JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Override
    @Async
    public void sendTextMail ( String to, String subject, String text) {
        sendM(to, subject, text, false);
//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setTo(to);
//        message.setSubject(subject);
//        message.setText(text);
//        emailSender.send(message);
    }

    @Override
    @Async
    public void sendTextMail (Mail mail) {
        sendM(mail.getTo(), mail.getSubject(), mail.getText(), false);
    }

    @Override
    @Async
    public void sendHtmlMail( String to, String subject, String text) {
        sendM(to, subject, text, true);
    }

    @Override
    @Async
    public void sendHtmlMail( Mail mail) {
        sendM(mail.getTo(), mail.getSubject(), mail.getText(), true);
    }

    @Override
    @Async
    public void sendHtmlTemplateMail(String to, String subject, String templateName, Context context) {
        String text = templateEngine.process(templateName, context);
        System.out.println(text);
        sendM(to, subject, text, true);
    }

    @Override
    @Async
    public void sendHtmlTemplateMail(Mail mail, String templateName, Context context) {
        String text = templateEngine.process(templateName, context);
        System.out.println(text);
        sendM(mail.getTo(), mail.getSubject(), text, true);
    }

    public void sendMailWithInline(
            final String recipientName, final String recipientEmail, final String imageResourceName,
            final byte[] imageBytes, final String imageContentType, final Locale locale)
            throws MessagingException {

        // Prepare the evaluation context
        final Context ctx = new Context(locale);
        ctx.setVariable("name", recipientName);
        ctx.setVariable("subscriptionDate", LocalDateTime.now());
        ctx.setVariable("hobbies", Arrays.asList("Cinema", "Sports", "Music"));
        ctx.setVariable("imageResourceName", imageResourceName); // so that we can reference it from HTML

        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
        final MimeMessageHelper message =
                new MimeMessageHelper(mimeMessage, true, "UTF-8"); // true = multipart
        message.setSubject("Example HTML email with inline image");
        message.setFrom("thymeleaf@example.com");
        message.setTo(recipientEmail);

        // Create the HTML body using Thymeleaf
        final String htmlContent = this.templateEngine.process("email-inlineimage.html", ctx);
        message.setText(htmlContent, true); // true = isHtml

        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
        message.addInline(imageResourceName, imageSource, imageContentType);

        // Send mail
        this.javaMailSender.send(mimeMessage);

    }

    private void sendM(String to, String subject, String text, Boolean isHtml){

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(
                    mimeMessage, false, StandardCharsets.UTF_8.name());
            //messageHelper.setFrom("sample@dolszewski.com");
            messageHelper.setText(text, isHtml);
            messageHelper.setText(new String(text.getBytes(),"UTF-8"), isHtml);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(text);
        };

        try {
            javaMailSender.send(messagePreparator);
            LOGGER.info("Send email '{}' to: {}", subject, to);
        } catch (MailException e) {
            LOGGER.error(String.format("Problem with sending email to: {}, error message: {}", to, e.getMessage()));
             //runtime exception; compiler will not force you to handle it
        }
    }

//    @Override
//    public void sendMessageWithAttachment( String to, String subject, String text, String pathToAttachment) {
//        MimeMessage message = emailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message, true);
//        helper.setTo(to);
//        helper.setSubject(subject);
//        helper.setText(text);
//        FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
//        helper.addAttachment("Invoice", file);
//        emailSender.send(message);
//    }

}

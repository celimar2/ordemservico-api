package br.net.gradual.core.service.impl;

import br.net.gradual.business.model.Colaborador;
import br.net.gradual.core.dao.PasswordTokenRepository;
import br.net.gradual.core.dao.UserDao;
import br.net.gradual.core.model.*;
import br.net.gradual.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime  ;
import java.util.*;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

    private UserDao userDao;
    private BCryptPasswordEncoder bcryptEncoder;
    private PasswordTokenRepository pwdResetRepo;

    @Autowired
    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bcryptEncoder, PasswordTokenRepository passwordTokenRepository) {
        this.userDao = userDao;
        this.bcryptEncoder = bcryptEncoder;
        this.pwdResetRepo = passwordTokenRepository;
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(role -> {
            //authorities.add(new SimpleGrantedAuthority(role.getName()));
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        });
        return authorities;
    }

    @Override
    public List<RoleMenu> getMenuAccess(User user) {
//        System.out.println("================================================================");
//        System.out.println("user: "+ user.getUsername());
        List<RoleMenu> roleMenuList = new ArrayList<RoleMenu>();
        user.getRoles().forEach(role -> {
//            System.out.println("================================================================");
//            System.out.println("role: "+role.getName());
            role.getRoleMenuList().forEach(itm -> {
                Optional<RoleMenu> oRM = roleMenuList.stream()
                        .filter(roleMenu -> roleMenu.getMenu().getName().equals( itm.getMenu().getName()) )
                        .findFirst();
                if (! oRM.isPresent()) {
                    roleMenuList.add(itm);
//                    System.out.println(itm.toString());
//                    System.out.println(itm.toString());
                } else {
                    RoleMenu rm = oRM.get();
                    rm.setCreate(itm.isCreate() ? itm.isCreate() : rm.isCreate());
                    rm.setRead(itm.isRead() ? itm.isRead() : rm.isRead() );
                    rm.setUpdate(itm.isUpdate() ? itm.isUpdate() : rm.isUpdate());
                    rm.setDelete(itm.isDelete() ? itm.isDelete() : rm.isDelete());
//                    System.out.println(rm.toString());
                }
            });
        });
        return roleMenuList;
    }

//    private boolean hasChild(RoleMenu itm, List<RoleMenu> menu) {
//        if (itm.getMenu() != null) {
//            for (RoleMenu rm : menu) {
//                Menu x = rm.getMenu().getParentMenu();
//                if ((x != null) &&(x.getId() == itm.getMenu().getId() )) {
//                   return true;
//                }
//            }
//        }
//        return false;
//    }

    @Override
    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        userDao.deleteById(id);
    }

    @Override
    public User findOne(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public User findByColaborador(Colaborador colaborador) {
        return userDao.findByColaborador(colaborador);
    }

    @Override
    public List<User> listAllNoColaborador() {
        return userDao.findAllByColaboradorIsNull();
    }

    @Override
    public User findById(Long id) {
        return userDao.findById(id).get();
    }

    @Override
    public void saveRoles(Long id, List<Role> roles) {
        userDao.findById(id).get().setRoles(roles);
    }

    @Override
    public User save(UserDto user) {

        Boolean changePwd = false;
        LocalDateTime pwdChangedNow = user.getPasswordAltered();
        String pwd = user.getPassword();
        if (user.getId() == 0) {
            changePwd = true;
        } else if ((pwd != null) && (!pwd.isEmpty()) && (user.getId() > 0)) {
            if (userDao.findById(user.getId()).isPresent()) {
                changePwd = !userDao.findById(user.getId()).get().getPassword().equals(pwd);
            }
        }
        if (changePwd) {
            pwdChangedNow = LocalDateTime.now();
            pwd = bcryptEncoder.encode(pwd);
        }

        User newUser = new User();
        newUser.setId(user.getId());
        newUser.setUsername(user.getUsername());
        newUser.setEmail(user.getEmail());
        newUser.setPassword(pwd);
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setActive(user.getActive());
        newUser.setBlocked(user.getBlocked());
        newUser.setLoginAttempts(user.getLoginAttempts());
        newUser.setChangePasswordNextLogin(user.getChangePasswordNextLogin());
        newUser.setLastLogin(user.getLastLogin());
        newUser.setPasswordAltered(pwdChangedNow);
        newUser.setRoles(user.getRoles());

//        User userAux = userDao.save(newUser);
//        userAux.setRoles(user.getRoles());

        return userDao.save(newUser);
    }

    @Override
    public void setLoginAttempts(String username, boolean loginSuccess) {
        User user = userDao.findByUsername(username);
        if (user != null) {

            if (loginSuccess) {
                user.setLoginAttempts(0);
            } else {
                user.setLoginAttempts(user.getLoginAttempts() + 1);
            }
            user.setLastLogin(LocalDateTime.now());
            userDao.save(user);
        }
    }

    @Override
    public void setLastLogin(String username) {

    }

    @Override
    public void createPasswordResetTokenForUser(User user, String token) {
        PasswordResetToken pwdRstToken = new PasswordResetToken(token, user);
        pwdResetRepo.save(pwdRstToken);
    }

    @Override
    public User validatePasswordResetToken(Long id, String token) {
        PasswordResetToken pwdRstToken = pwdResetRepo.findByToken(token);
        if (pwdRstToken.isUserId(id) && !pwdRstToken.isExpired()) {
            return pwdRstToken.getUser();
        }
        return null;
    }

    private class MenuItem {
        public String key;
        public String value;

        public MenuItem(String key, String value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof MenuItem)) return false;
            MenuItem menuItem = (MenuItem) o;
            return Objects.equals(key, menuItem.key);
        }

        @Override
        public int hashCode() {

            return Objects.hash(key);
        }
    }
}
package br.net.gradual.core.service;

import br.net.gradual.core.model.Mail;
import org.thymeleaf.context.Context;

public interface EmailService {

    void sendTextMail ( String to, String subject, String text);
    void sendTextMail (Mail mail);
    void sendHtmlMail( String to, String subject, String text);
    void sendHtmlMail( Mail mail) ;
    void sendHtmlTemplateMail(String to, String subject, String templateName, Context context) ;
    void sendHtmlTemplateMail(Mail mail, String templateName, Context context);

}

package br.net.gradual.core.service.impl;

import br.net.gradual.core.dao.RoleDao;
import br.net.gradual.core.model.Role;
import br.net.gradual.core.model.RoleDto;
import br.net.gradual.core.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service(value = "roleService")
public class RoleServiceImpl implements RoleService {

    private RoleDao roleDao;
    private BCryptPasswordEncoder bCryptEncoder;

    @Autowired
    public RoleServiceImpl(RoleDao roleDao, BCryptPasswordEncoder bCryptEncoder) {
        this.roleDao = roleDao;
        this.bCryptEncoder = bCryptEncoder;
    }

    @Override
    public List<Role> findAll() {
        List<Role> list = new ArrayList<>();
        roleDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<Role> findAllByActive(Boolean active) {
        List<Role> list = new ArrayList<>();
        roleDao.findAllByActive(active).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        roleDao.deleteById(id);
    }

    @Override
    public Role findOne(String name) {
        return roleDao.findByName(name);
    }

    @Override
    public Role findById(Long id) {
        return roleDao.findById(id).get();
    }

    @Override
    public Role save(RoleDto role) {
        Role newRole = new Role();
        newRole.setId(role.getId());
        newRole.setName(role.getName());
        newRole.setDescription(role.getDescription() );
        newRole.setActive(role.getActive());
        newRole.setRoleMenuList(role.getRoleMenuList());
        return roleDao.save(newRole);
    }

}

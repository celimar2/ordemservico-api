package br.net.gradual.core.service;

import br.net.gradual.core.model.Role;
import br.net.gradual.core.model.RoleDto;

import java.util.List;

public interface RoleService {

    Role save(RoleDto role);

    List<Role> findAll();
    List<Role> findAllByActive(Boolean active);

    void delete(long id);

    Role findOne(String name);

    Role findById(Long id);

}
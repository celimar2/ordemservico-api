package br.net.gradual.core.controller;

import br.net.gradual.business.repository.VeiculoRepository;
import br.net.gradual.core.config.TokenProvider;
import br.net.gradual.core.error.ResourceNotFoundException;
import br.net.gradual.core.model.AuthToken;
import br.net.gradual.core.model.LoginUser;
import br.net.gradual.core.model.ResponseMessage;
import br.net.gradual.core.model.User;
import br.net.gradual.core.service.EmailService;
import br.net.gradual.core.service.UserService;
import net.sf.jasperreports.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.context.Context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private VeiculoRepository veiculoDAO;

    private static final String template = " Ping - > pong!";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PDFReportGenerator.class);
	 

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> authenticate(@RequestBody LoginUser loginUser) throws AuthenticationException {
//        System.out.println(loginUser.toString());
        userService.setLoginAttempts(loginUser.getUsername(), false);
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userService.setLoginAttempts(loginUser.getUsername(), true);
        final String token = jwtTokenUtil.generateToken(authentication);
//        System.out.println("token: " + token);
        return ResponseEntity.ok(new AuthToken(token));
    }

    @RequestMapping(value = "test/ping", method = RequestMethod.GET)
    public String pingPong() throws AuthenticationException {
        return template;
    }



//    @RequestMapping(path = "/pdf", method = RequestMethod.GET)
//    public ModelAndView report() {
//Jasper
//        JasperReportsPdfView view = new JasperReportsContext() PdfView();
//        view.setUrl("classpath:templates/reports/veiculos.jrxml");
//        view.setApplicationContext(appContext);
//
//        Map<String, Object> params = new HashMap<>();
//        params.put("datasource", carService.findAll());
//
//        return new ModelAndView(view, params);
//    }

//
//    @RequestMapping(value = "helloReport1", method = RequestMethod.GET)
//    @ResponseBody
//    public void getRpt1(HttpServletResponse response) throws JRException, IOException {
//        InputStream jasperStream = this.getClass().getResourceAsStream("/jasperreports/HelloWorld1.jasper");
//        Map<String,Object> params = new HashMap<>();
//        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
//        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
//
//        response.setContentType("application/x-pdf");
//        response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");
//
//        final OutputStream outStream = response.getOutputStream();
//        JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
//    }


    @RequestMapping(value = "test/rpt", method = RequestMethod.GET)
    @ResponseBody
    public void getRpt1(HttpServletResponse response) throws JRException, IOException {
//        System.out.println("111111111");
        InputStream jrxmlStream = this.getClass().getResourceAsStream("veiculos.jrxml");
//        System.out.println(jrxmlStream.read());
//        System.out.println("2 -------------------");
        Map params = new HashMap<>();
//        params.put("datasource", veiculoDAO.findAll());
//        System.out.println("3 -------------------");

        JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlStream);
//        System.out.println("4 -------------------");

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
//        System.out.println("5 -------------------");

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");
        System.out.println("6 -------------------");

        final OutputStream outStream = response.getOutputStream();
        System.out.println("6 -------------------");
        JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
        System.out.println("7 -------------------");

    }
	
	@RequestMapping(value = "test/rpt2", method = RequestMethod.GET)
    @ResponseBody
	public void generate(Map parameters, String sourceFileName, String destFileName) {
 
        JasperReport jasperReport;
        JasperPrint jasperPrint;
 
        try {
            jasperReport = JasperCompileManager.compileReport(sourceFileName);
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfFile(jasperPrint, destFileName);
        } catch (JRException e) {
            LOGGER.error("Problem with report generation. Template:'{}',  Parameters:{}", sourceFileName, parameters);
        }
 
    }

    @RequestMapping(value = "/reset-password", method = RequestMethod.POST)
//    public ResponseEntity<?> resetPassword(HttpServletRequest request, @RequestParam("email") String emailTo) throws ResourceNotFoundException {
//    @RequestMapping(value = "/reset-password/{email}", method = RequestMethod.POST)
    public ResponseEntity<?> resetPassword(HttpServletRequest request, @RequestBody String emailTo) throws ResourceNotFoundException {

        System.out.println("resetPassword: " + emailTo);
        User user = userService.findByEmail(emailTo);
        if (user == null) {
//            throw new ResourceNotFoundException("Nenhum usuário localizado com email: '" + emailTo + "'");
            return ResponseEntity.ok(new ResponseMessage("Nenhum usuário localizado com email: '" + emailTo + "'", HttpStatus.NOT_FOUND));
        } else {

            try {

                String token = UUID.randomUUID().toString();
                userService.createPasswordResetTokenForUser(user, token);
                String url = URLEncoder.encode(
                        "https://localhost:4200/reset-password" +
                                "?id=" + user.getId() +
                                "&mail=" + emailTo +
                                "&token=" + token, "UTF-8");
                Context context = new Context();
                context.setVariable("firstName", user.getFirstName());
                context.setVariable("lastName", user.getLastName());
                context.setVariable("resetUrl", url);
                context.setVariable("signature", "[assinatura]");
                String subject = "Reset Password";
//                emailService.sendHtmlTemplateMail(emailTo, subject, "/mail/html/resetpwd", context);
                emailService.sendHtmlTemplateMail(emailTo, subject, "/mail/html/reset", context);

                String mailTitle = "Gradual - Controle de Serviços - Reset password";
                String mailText = "Hey! \n " +
                        "Just a mail test to reset password. \n" +
                        "Please do not respond this! \n " +
                        "token: " + token + ". \n " +
                        "url: " + url + ". \n " +
                        "Thank you. \n " +
                        "Cheers! \n\n " +
                        "Gradual Dev Team.";

                emailService.sendTextMail(emailTo, mailTitle, mailText);

                return ResponseEntity.ok(new ResponseMessage("Foi enviado um email para '" + emailTo + "' com instruções para alterar sua senha.",
                        HttpStatus.OK));

            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok(new ResponseMessage("Erro ao enviar e-mail", HttpStatus.INTERNAL_SERVER_ERROR));
            }

        }
    }

//    @RequestMapping(value = "/url", method = RequestMethod.GET)
//    public ResponseEntity<?> getURLValue(HttpServletRequest request) throws IOException {
//        List<String> test = new ArrayList<>();
//        test.add("getRequestURI: " + request.getRequestURI());
//        test.add("getRequestURL(): " + request.getRequestURL() );
//        test.add("getRequestURL(): " + request.getRequestURL().toString().replace(request.getRequestURI(), "") );
//        test.add("getServletPath(): " + request.getServletPath() );
//        test.add("getContextPath(): " + request.getContextPath());
//        test.add("getQueryString(): " + request.getQueryString() );
//        test.add("getPathInfo(): " + request.getPathInfo() );
//        test.add("getPathTranslated(): " + request.getPathTranslated() );
//        test.add("getServerName(): " + request.getServerName() );
////        test.add("getHeaderNames(): " + request.getHeaderNames() );
//        test.add("getLocalAddr(): " + request.getLocalAddr() );
//        test.add("getInputStream().toString(): " + request.getInputStream().toString());
//        return new ResponseEntity<>(test, HttpStatus.OK);
//    }


    @RequestMapping(value = "/valid-rst-pwd", params = {"id", "tkn"}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> validResetPasswordToken(@RequestParam("id") long id, @RequestParam("tkn") String token) {
        User user = userService.validatePasswordResetToken(id, token);

        if (user != null) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ResponseMessage("Token não encontrad"), HttpStatus.NOT_FOUND);
    }
	
	

}

package br.net.gradual.core.controller;

import br.net.gradual.business.model.collections.Genero;
import br.net.gradual.business.model.collections.SituacaoVeiculo;
import br.net.gradual.business.model.collections.TipoJuridico;
import br.net.gradual.business.model.collections.TipoTelefone;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/enums")
public class EnumsController {

    @GetMapping(value = "/tipos-telefone")
    public ResponseEntity<?> getTiposTelefone() throws AuthenticationException {
        Map<String,String> map = new HashMap<String, String>();
        for (TipoTelefone tt : TipoTelefone.values()) {
            map.put(tt.getValor(), tt.toString());
        }
        return new ResponseEntity( map, HttpStatus.OK);
    }

    @GetMapping(value = "/tipos-juridico")
    public ResponseEntity<?> getTiposJuridico() throws AuthenticationException {
        Map<String,String> map = new HashMap<String, String>();
        for (TipoJuridico tj: TipoJuridico.values()) {
            map.put(tj.getValor(), tj.toString());
        }
        return new ResponseEntity( map, HttpStatus.OK);
    }

    @GetMapping(value = "/generos")
    public ResponseEntity<?> getGeneros() throws AuthenticationException {
        Map<String,String> map = new HashMap<String, String>();
        for (Genero gs: Genero.values()) {
            map.put(gs.getValor(), gs.toString());
        }
        return new ResponseEntity( map, HttpStatus.OK);
    }

    @GetMapping(value = "/situacoes-veiculos")
    public ResponseEntity<?> getSituacaoVeiculo() throws AuthenticationException {
        Map<String,String> map = new HashMap<String, String>();
        for (SituacaoVeiculo sv: SituacaoVeiculo.values()) {
            map.put(sv.getValor(), sv.toString());
        }
        return new ResponseEntity( map, HttpStatus.OK);
    }

}

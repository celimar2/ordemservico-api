package br.net.gradual.core.controller;

import br.net.gradual.core.dao.MenuRepository;
import br.net.gradual.core.dao.PageRepository;
import br.net.gradual.core.dao.RolePageRepository;
import br.net.gradual.core.model.Role;
import br.net.gradual.core.model.RoleDto;
import br.net.gradual.core.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private RolePageRepository rolePageRepository;

    @GetMapping
    public ResponseEntity<?>listAll(){
        List<Role> list = new ArrayList<>();
        list.addAll(roleService.findAll());
        list.forEach(role ->
                role.getRoleMenuList().clear()
        );
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/ativos")
    public ResponseEntity<?> listAllAtivos() {
        return new ResponseEntity<>(roleService.findAllByActive(true), HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") Long id) {
        return new ResponseEntity<>(roleService.findById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<?> getOne(@PathVariable("name") String name) {
        return new ResponseEntity<>(roleService.findOne(name), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/menus")
    public ResponseEntity<?> getMenu(@PathVariable("id") Long id) {
        return new ResponseEntity<>(roleService.findById(id).getRoleMenuList(), HttpStatus.OK);
    }
//
    @GetMapping(value = "/{id}/pages")
    public ResponseEntity<?> getPages(@PathVariable("id") Long id) {
        return new ResponseEntity<>(roleService.findById(id).getRolePageList(), HttpStatus.OK);
    }

    @GetMapping(value = "/menus")
    public ResponseEntity<?> getNewMenu() {
        return new ResponseEntity<>(menuRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/pages")
    public ResponseEntity<?> getNewPage() {
        return new ResponseEntity<>(pageRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> saveRole(@RequestBody RoleDto role){
        return new ResponseEntity<>(roleService.save(role), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> updateRole(@RequestBody RoleDto role){
        if (roleService.findOne(role.getName()) != null)
            return new ResponseEntity<>(roleService.save(role), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<?> deleterole(@PathVariable("id") Long id){
        if (roleService.findById(id) != null) {
            roleService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}

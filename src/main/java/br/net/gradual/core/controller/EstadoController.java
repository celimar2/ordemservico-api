package br.net.gradual.core.controller;

import br.net.gradual.core.dao.EstadoRepository;
import br.net.gradual.core.model.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/estados")
public class EstadoController {

    private final EstadoRepository estadoDAO;

    @Autowired
    public EstadoController(EstadoRepository estadoDAO) {
        this.estadoDAO = estadoDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(estadoDAO.findAll(), HttpStatus.OK);
    }

//    public ResponseEntity<?> listAllPagging(Pageable pageable) {
//        return new ResponseEntity<>(estadoDAO.findAll(pageable), HttpStatus.OK);
//    }

    @GetMapping(path="/{uf}")
    public ResponseEntity<?> getById(@PathVariable("uf") String uf) {
        return new ResponseEntity<>(estadoDAO.findById(uf.toUpperCase()).get(), HttpStatus.OK);
    }

    @GetMapping(path="/{uf}/municipios")
    public ResponseEntity<?> getMunicipioByUf(@PathVariable("uf") String uf) {
        return new ResponseEntity<>(estadoDAO.findById(uf.toUpperCase()).get().getMunicipioList(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody Estado estado) {
        return new ResponseEntity<>(estadoDAO.save(estado), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Estado estado) {
        return new ResponseEntity<>(estadoDAO.save(estado), HttpStatus.OK);
    }

    @DeleteMapping("/{uf}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("uf") String uf) {
        estadoDAO.deleteById(uf.toUpperCase());
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

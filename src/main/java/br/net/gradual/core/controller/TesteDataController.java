package br.net.gradual.core.controller;

import br.net.gradual.core.dao.TesteDataRepository;
import br.net.gradual.core.model.TesteData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/testes")
public class TesteDataController {

    private final TesteDataRepository testeDataDao;

    @Autowired
    public TesteDataController(TesteDataRepository testeDataDao) {
        this.testeDataDao = testeDataDao;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(testeDataDao.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(testeDataDao.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody TesteData testeData) {
        System.out.println(testeData.toString());
        return new ResponseEntity<>(testeDataDao.save(testeData), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody TesteData testeData) {
        return new ResponseEntity<>(testeDataDao.save(testeData), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        testeDataDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

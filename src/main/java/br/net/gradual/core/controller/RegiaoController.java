package br.net.gradual.core.controller;

import br.net.gradual.core.dao.RegiaoRepository;
import br.net.gradual.core.model.Regiao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/regioes")
public class RegiaoController {

    private final RegiaoRepository regiaoDAO;

    @Autowired
    public RegiaoController(RegiaoRepository regiaoDAO) {
        this.regiaoDAO = regiaoDAO;
    }

    @GetMapping
//    public ResponseEntity<?> listAll(Pageable pageable) {
//        return new ResponseEntity<>(regiaoDAO.findAll(pageable), HttpStatus.OK);
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(regiaoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
//        verifyIfExists(id);
        return new ResponseEntity<>(regiaoDAO.findById(id).get(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}/estado")
    public ResponseEntity<?> getEstadoById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(regiaoDAO.findById(id).get().getEstadoList(), HttpStatus.OK);
    }

//    @GetMapping(path="/findByNome/{nome}")
//    public ResponseEntity<?> getByNome(String nome) {
//        List<Regiao> regiaoList = regiaoDAO.findByNomeIgnoreCase(nome);
//        return new ResponseEntity<>(regiaoList, HttpStatus.OK);
//    }

//    @GetMapping(path="/getEstadoByRegiao/{id}")
//    public ResponseEntity<?> getEstadoByRegiao(@PathVariable("id") Long id) {
//        verifyIfExists(id);
//        List<Estado> estadoList =  regiaoDAO.findById(id).get().getEstadoList();
//        return new ResponseEntity<>(estadoList, HttpStatus.OK);
//    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody Regiao regiao) {
        return new ResponseEntity<>(regiaoDAO.save(regiao), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Regiao regiao) {
//        verifyIfExists(regiao.getId());
        return new ResponseEntity<>(regiaoDAO.save(regiao), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
//        verifyIfExists(id);
        regiaoDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

//    private void verifyIfExists(Long id) {
//        if (! regiaoDAO.existsById(id) )
//            throw new ResourceNotFoundException("Regiao not found for id:"+id);
//    }

}

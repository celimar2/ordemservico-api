package br.net.gradual.core.controller;

import br.net.gradual.business.model.Colaborador;
import br.net.gradual.business.repository.ColaboradorRepository;
import br.net.gradual.core.model.Role;
import br.net.gradual.core.model.User;
import br.net.gradual.core.model.UserDto;
import br.net.gradual.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ColaboradorRepository colaboradorRepository;

    @GetMapping
    public ResponseEntity<?>listAll(){
        List<User> list = new ArrayList<>();
        list.addAll(userService.findAll());
        list.forEach( user -> user.getRoles().clear());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/colaborador/{id}")
    public ResponseEntity<?>listByColaborador(@PathVariable("id") Long id){
        Colaborador colaborador = colaboradorRepository.findById(id).get();
        return new ResponseEntity<>(userService.findByColaborador(colaborador), HttpStatus.OK);
    }

    @GetMapping(value = "/no-colaborador")
    public ResponseEntity<?>listAllNoColaborador(){
        return new ResponseEntity<>(userService.listAllNoColaborador(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        user.getRoles().forEach(role -> role.getRoleMenuList().clear() );
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
    @GetMapping(value = "/access/{username}")
    public ResponseEntity<?> getUserAccess(@PathVariable("username") String username) {
//        System.out.println("/access/{username}: "+username);
        User user = userService.findOne(username);
//        System.out.println("user: "+user.toString());
        return new ResponseEntity<>(userService.getMenuAccess(user), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/roles")
    public ResponseEntity<?>listRolesByUserId(@PathVariable("id") Long id){
        List<Role> list = new ArrayList<>();
        list.addAll(userService.findById(id).getRoles());
        list.forEach( role -> role.getRoleMenuList().clear());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/roles/{roleId}")
    public ResponseEntity<?> getOne(@PathVariable("id") Long id, @PathVariable("roleId") Long roleId) {
        Role role = userService.findById(id)
                .getRoles()
                .stream()
                .filter(role1 -> role1.getId().equals(roleId))
                .findFirst().get();
        role.getRoleMenuList().clear();
        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<?> signup(@RequestBody UserDto user){
        return new ResponseEntity<>(userService.save(user), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> saveUser(@RequestBody UserDto user){
        User _user = userService.save(user);
        _user.getRoles().clear();
        return new ResponseEntity<>(_user, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> updateUser(@RequestBody UserDto user){
        if (userService.findOne(user.getUsername()) != null) {
            user.getRoles().forEach(role -> role.getRoleMenuList().clear());
            return new ResponseEntity<>(userService.save(user), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
        if (userService.findById(id) != null) {
            userService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}

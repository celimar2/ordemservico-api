package br.net.gradual.core.controller;

import net.sf.jasperreports.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
 
public class PDFReportGenerator {
 
    private static final Logger LOGGER = LoggerFactory.getLogger(PDFReportGenerator.class);
 
    public void generate(Map parameters, String sourceFileName, String destFileName) {
 
        JasperReport jasperReport;
        JasperPrint jasperPrint;
 
        try {
            jasperReport = JasperCompileManager.compileReport(sourceFileName);
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfFile(jasperPrint, destFileName);
        } catch (JRException e) {
            LOGGER.error("Problem with report generation. Template:'{}',  Parameters:{}", sourceFileName, parameters);
        }
 
    }
 
}
package br.net.gradual.core.controller;

import br.net.gradual.core.dao.MunicipioRepository;
import br.net.gradual.core.model.Municipio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/municipios")
public class MunicipioController {

    private final MunicipioRepository municipioDAO;

    @Autowired
    public MunicipioController(MunicipioRepository municipioDAO) {
        this.municipioDAO = municipioDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(municipioDAO.findAll(), HttpStatus.OK);
    }

//    @GetMapping
//    public ResponseEntity<?> listAllPagging(Pageable pageable) {
//        return new ResponseEntity<>(municipioDAO.findAll(pageable), HttpStatus.OK);
//    }

//    public ResponseEntity<?> listAll(Pageable pageable) {
//        return new ResponseEntity<>(estadoDAO.findAll(pageable), HttpStatus.OK);
//    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(municipioDAO.findById(id).get(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}/estado")
    public ResponseEntity<?> getEstadoById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(municipioDAO.findById(id).get().getEstado(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody Municipio municipio) {
        return new ResponseEntity<>(municipioDAO.save(municipio), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Municipio municipio) {
        return new ResponseEntity<>(municipioDAO.save(municipio), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        municipioDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

package br.net.gradual.core;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@ConfigurationProperties(prefix = "reporting")
public class ApplicationProperties {

    /* The base path where reports will be stored after compilation */
    private Resource storageLocation;

    /*  The location of JasperReports source files */
    private Resource reportLocation;

    public Resource getStorageLocation() {
        return storageLocation;
    }

    public Resource getReportLocation() {
        return reportLocation;
    }
}
package br.net.gradual.core.dao;

import br.net.gradual.core.model.Page;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PageRepository extends PagingAndSortingRepository<Page, Long> {
    List<Page> findAll();
}

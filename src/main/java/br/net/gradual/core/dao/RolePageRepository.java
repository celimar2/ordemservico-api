package br.net.gradual.core.dao;

import br.net.gradual.core.model.RolePage;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RolePageRepository extends PagingAndSortingRepository<RolePage, Long> {
}

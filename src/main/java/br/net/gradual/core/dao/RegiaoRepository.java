package br.net.gradual.core.dao;

import br.net.gradual.core.model.Regiao;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RegiaoRepository extends PagingAndSortingRepository<Regiao, Long> {
}

package br.net.gradual.core.dao;

import br.net.gradual.core.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleDao extends CrudRepository<Role, Long> {
    Role findByName(String name);
    List<Role> findAllByActive(Boolean active);
}

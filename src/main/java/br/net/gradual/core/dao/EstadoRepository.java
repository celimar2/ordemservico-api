package br.net.gradual.core.dao;

import br.net.gradual.core.model.Estado;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EstadoRepository extends PagingAndSortingRepository<Estado, String> {
}

package br.net.gradual.core.dao;

import br.net.gradual.core.model.TesteData;
import org.springframework.data.repository.CrudRepository;

public interface TesteDataRepository extends CrudRepository<TesteData, Long> {
}

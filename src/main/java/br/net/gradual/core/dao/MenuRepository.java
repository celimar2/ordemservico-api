package br.net.gradual.core.dao;

import br.net.gradual.core.model.Menu;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MenuRepository extends PagingAndSortingRepository<Menu, Long> {
    List<Menu> findAll();
}

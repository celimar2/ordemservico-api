package br.net.gradual.core.dao;

import br.net.gradual.core.model.Regiao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegiaoDao extends CrudRepository<Regiao, Long> {
}

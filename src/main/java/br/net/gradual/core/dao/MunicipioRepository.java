package br.net.gradual.core.dao;

import br.net.gradual.core.model.Municipio;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MunicipioRepository extends PagingAndSortingRepository <Municipio, Long> {
}

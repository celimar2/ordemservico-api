package br.net.gradual.core.dao;

import br.net.gradual.business.model.Colaborador;
import br.net.gradual.core.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);
    User findByColaborador(Colaborador colaborador);
    List<User> findAllByColaboradorIsNull();
}

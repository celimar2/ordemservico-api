package br.net.gradual.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Regiao", uniqueConstraints={
        @UniqueConstraint(name="UK_Regiao_nome", columnNames="nome")})
public class Regiao extends AbstractEntity {

    @NotEmpty
    @Column(nullable = false, length = 50)
    private String nome;

    @OneToMany(mappedBy = "regiao", cascade = CascadeType.REMOVE)
    private List<Estado> estadoList = new ArrayList<Estado>();

    public Regiao() {
    }

    public Regiao(String nome) {
        this.setNome(nome);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase();
    }

    @JsonIgnore
    public List<Estado> getEstadoList() {
        return estadoList;
    }

    @JsonIgnore
    public void addEstado(Estado estado) {
        if (estado != null) {
            this.estadoList.add(estado);
            estado.setRegiao(this);
        }
    }

    @JsonIgnore
    public void removeEstado(Estado estado) {
        if (estado != null) {
            this.estadoList.remove(estado);
            //estado.setRegiao(null);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Regiao regiao = (Regiao) o;
        return Objects.equals(nome, regiao.nome);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), nome);
    }


}

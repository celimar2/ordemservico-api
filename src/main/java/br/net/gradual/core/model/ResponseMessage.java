package br.net.gradual.core.model;

import org.springframework.http.HttpStatus;

public class ResponseMessage {
    private String message;
    private HttpStatus error;

    public ResponseMessage(String s) {
    }

    public ResponseMessage(String message, HttpStatus error) {
        this.message = message;
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getError() {
        return error;
    }

    public void setError(HttpStatus error) {
        this.error = error;
    }
}

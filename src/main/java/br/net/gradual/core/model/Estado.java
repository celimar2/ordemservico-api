package br.net.gradual.core.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Estado", uniqueConstraints={
        @UniqueConstraint(name="UK_Estado_Nome", columnNames="nome"),
        @UniqueConstraint(name="UK_Estado_Uf", columnNames="uf")})
public class Estado {

    @Id
    @Column(columnDefinition = "CHAR(2)", length = 2)
    private String uf;

    @NotEmpty
    @Column(length = 50)
    private String nome;

//    @NotEmpty
    private int codigoIbge;

    @ManyToOne
    @JoinColumn(name = "regiaoId", referencedColumnName = "id",
            foreignKey=@ForeignKey(name = "Fk_Estado_Regiao"))
    private Regiao regiao;

    @JsonIgnore
    @OneToMany(mappedBy = "estado", cascade = CascadeType.REMOVE)
    private List<Municipio> municipioList = new ArrayList<>();

//    @OneToMany(mappedBy = "estado", cascade = CascadeType.REMOVE)
//    private List<Endereco> enderecoList = new ArrayList<>();

    public Estado() {
    }

    public Estado(String uf, @NotEmpty String nome) {
        this.uf = uf;
        this.nome = nome;
    }

    public Estado(String uf, @NotEmpty String nome, Regiao regiao) {
        this.uf = uf;
        this.nome = nome;
        this.regiao = regiao;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf.toUpperCase();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase();
    }

    public int getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(int codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public Regiao getRegiao() {
        return regiao;
    }

    public void setRegiao(Regiao regiao) {
        if (regiao == null ) {
            if (this.regiao != null)
                this.regiao.removeEstado(this);
        } else {
            if (regiao.getEstadoList().indexOf(this) == -1)
                regiao.addEstado(this);
        }
        this.regiao = regiao;
    }
//    @JsonIgnore
//    public List<Endereco> getEnderecoList() {
//        return enderecoList;
//    }
//
//    public void setEnderecoList(List<Endereco> enderecoList) {
//        this.enderecoList = enderecoList;
//    }

    @JsonIgnore
    public List<Municipio> getMunicipioList() {
        return municipioList;
    }

    @JsonIgnore
    public void addMunicipio(Municipio municipio) {
        if (municipio != null) {
            this.municipioList.add(municipio);
            municipio.setEstado(this);
        }
    }

    @JsonIgnore
    public void removeMunicipio(Municipio municipio) {
        if (municipio != null) {
            this.municipioList.remove(municipio);
            municipio.setEstado(null);
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Estado estado = (Estado) o;
        return Objects.equals(uf, estado.uf);
    }

    @Override
    public int hashCode() {

        return Objects.hash(uf);
    }
}

package br.net.gradual.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "role_menu")
public class RoleMenu extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "menuId", referencedColumnName = "id", foreignKey = @ForeignKey(name = "Fk_role_menu_menu"))
    private Menu menu;

    @Column(name = "cr", nullable = false, columnDefinition = "Boolean DEFAULT 0")
    private boolean create;

    @Column(name = "rd", nullable = false, columnDefinition = "Boolean DEFAULT 0")
    private boolean read;

    @Column(name = "up", nullable = false, columnDefinition = "Boolean DEFAULT 0")
    private boolean update;

    @Column(name = "dl", nullable = false, columnDefinition = "Boolean DEFAULT 0")
    private boolean delete;


    public RoleMenu() {
    }

    public RoleMenu(RoleMenu roleMenu) {
        this.menu = roleMenu.menu;
        this.create = roleMenu.create;
        this.read = roleMenu.read;
        this.update = roleMenu.update;
        this.delete = roleMenu.delete;
    }

    public RoleMenu(Menu menu, boolean create, boolean read, boolean update, boolean delete) {
        this.menu = menu;
        this.create = create;
        this.read = read;
        this.update = update;
        this.delete = delete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoleMenu)) return false;
        if (!super.equals(o)) return false;

        RoleMenu roleMenu = (RoleMenu) o;
//        System.out.println("o:" +roleMenu.getMenu().getName() +
//                " - " + menu.getName() +
//                "  -->  "+ getMenu().equals(roleMenu.getMenu()));
//        System.out.println("---------------------------------------------------------------");
        return getMenu().equals(roleMenu.getMenu());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getMenu().hashCode();
        return result;
    }

    public Menu getMenu() {
        return menu;
    }

    @JsonIgnore
    @Override
    public String toString() {
        return "RoleMenu{" + "menu=" + menu.getName() +
                ", create=" + create + ", read=" + read + ", update=" + update + ", delete=" + delete + '}';
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}

package br.net.gradual.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Page", uniqueConstraints = {
        @UniqueConstraint( columnNames = {"url"}, name="UK_Page_url") ,
        @UniqueConstraint( columnNames = {"url"}, name="UK_Page_title") })
public class Page extends AbstractEntity {
//    public static final String CRUD_CREATE_CODE = "C";
//    public static final String CRUD_READ_CODE = "R";
//    public static final String CRUD_UPDATE_CODE = "U";
//    public static final String CRUD_DELETE_CODE = "D";
//    public static final String EMPTY_STRING = "";

    @Column(length = 200)
    private String url;

    @Column(length = 100)
    private String title;

    @NotNull
    @Column(length = 5)
    private String crud; //string CRUD-Create,Read,Update,Delete,Execute

    @Column(columnDefinition = "boolean DEFAULT 1", nullable = false)
    private Boolean ativo;

    public Page() {
    }

    public Page(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCrud() {
        return crud;
    }

    public void setCrud(String crud) {
        this.crud = crud;
    }

//    @JsonIgnore
//    public Boolean getCreate() {
//        return crud.contains(CRUD_CREATE_CODE);
//    }
//    @JsonIgnore
//    public void setCreate(Boolean create) {
//        this.crud.replace(CRUD_CREATE_CODE, EMPTY_STRING);
//        if (create)
//            this.crud.concat(CRUD_CREATE_CODE);
//    }
//    @JsonIgnore
//    public Boolean getRead() {
//        return crud.contains(CRUD_READ_CODE);
//    }
//    @JsonIgnore
//    public void setRead(Boolean create) {
//        this.crud.replace(CRUD_READ_CODE, EMPTY_STRING);
//        if (create)
//            this.crud.concat(CRUD_READ_CODE);
//    }
//    @JsonIgnore
//    public Boolean getUpdate() {
//        return crud.contains(CRUD_UPDATE_CODE);
//    }
//    @JsonIgnore
//    public void setUpdate(Boolean update) {
//        this.crud.replace(CRUD_UPDATE_CODE, EMPTY_STRING);
//        if (update)
//            this.crud.concat(CRUD_UPDATE_CODE);
//    }
//    @JsonIgnore
//    public Boolean getDelete() {
//        return crud.contains(CRUD_DELETE_CODE);
//    }
//    @JsonIgnore
//    public void setDelete(Boolean delete) {
//        this.crud.replace(CRUD_DELETE_CODE, EMPTY_STRING);
//        if (delete)
//            this.crud.concat(CRUD_DELETE_CODE);
//    }
//    @JsonIgnore
//    public Boolean getExecute() {
//        return crud.contains(CRUD_DELETE_CODE);
//    }
//    @JsonIgnore
//    public void setExecute(Boolean execute) {
//        this.crud.replace(CRUD_EXECUTE_CODE, EMPTY_STRING);
//        if (execute)
//            this.crud.concat(CRUD_EXECUTE_CODE);
//    }

}

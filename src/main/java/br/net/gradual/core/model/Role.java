package br.net.gradual.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "role",
        uniqueConstraints = {@UniqueConstraint( columnNames = {"name"}, name="UK_Role_Name") })
public class Role extends AbstractEntity{

    private String name;

    @Column
    private String description;

    @NotNull
    @Column(columnDefinition = "boolean DEFAULT 1")
    private Boolean active;

//    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "role_menus",
            joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "FK_role_menu_Role"))},
            inverseJoinColumns = {@JoinColumn(name = "role_menu_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "FK_role_menus_role_menu")) })
    private List<RoleMenu> roleMenuList = new ArrayList<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_pages",
            joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "FK_role_pages_role"))},
            inverseJoinColumns = {@JoinColumn(name = "role_page_id", referencedColumnName = "id", foreignKey=@ForeignKey(name = "FK_role_pages_role_page")) })
    private List<RolePage> rolePageList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }


    public List<RoleMenu> getRoleMenuList() {
        return roleMenuList;
    }

    public void setRoleMenuList(List<RoleMenu> roleMenuList) {
        this.roleMenuList = roleMenuList;
    }

    @JsonIgnore
    public List<RolePage> getRolePageList() {
        return rolePageList;
    }

    @JsonIgnore
    public void setRolePageList(List<RolePage> rolePageList) {
        this.rolePageList = rolePageList;
    }

//    @JsonIgnore
//    public void clearRoleMenuList() {
//        this.setRoleMenuList(new ArrayList<>());
//    }
}

package br.net.gradual.core.model;

import javax.persistence.*;

@Entity
@Table(name = "role_page")
public class RolePage extends AbstractEntity {
    @ManyToOne
    @JoinColumn(name = "pageId", referencedColumnName = "id", foreignKey = @ForeignKey(name = "Fk_role_page_page"))
    private Page page;

    @Column(length = 5, nullable = false)
    private String crude; //string CRUD-Create,Read,Update,Delete,Execute

    public RolePage() {
    }

    public RolePage(Page page, String crude) {
        this.page = page;
        this.crude = crude;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getCrude() {
        return crude;
    }

    public void setCrude(String crude) {
        this.crude = crude;
    }

}

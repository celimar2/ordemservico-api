package br.net.gradual.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

@Entity
@Table(name = "municipio", uniqueConstraints={
        @UniqueConstraint(name="UK_Municipio_Uf_Nome", columnNames={"uf","nome"}),
        @UniqueConstraint(name="UK_Municipio_CodigoIbge", columnNames="codigoIbge")})
public class Municipio extends AbstractEntity {

    @NotEmpty
    @Column(length = 255)
    private String nome;

    private Long codigoIbge;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "uf", referencedColumnName = "uf",
            foreignKey=@ForeignKey(name = "Fk_Municipio_Estado"))
    private Estado estado;

//    @OneToMany(mappedBy = "municipio", cascade = CascadeType.REMOVE)
//    private List<Endereco> enderecoList = new ArrayList<>();

    public Municipio() {
    }

    public Municipio(Long id) {
        this.setId(id);
    }

    public Municipio(@NotEmpty String nome, Estado estado) {
        this.setNome(nome);
        this.setEstado(estado);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase();
    }

    public String getUf() {
        if (estado != null)
            return estado.getUf();
        return null;
    }

    public Long getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(Long codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    @JsonIgnore
    public Estado getEstado() {
        return estado;
    }

    @JsonIgnore
    public void setEstado(Estado estado) {
        if (estado == null ) {
            if (this.estado != null)
                this.estado.removeMunicipio(this);
        } else {
            if (estado.getMunicipioList().indexOf(this) == -1)
                estado.addMunicipio(this);
        }
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Municipio municipio = (Municipio) o;
        return Objects.equals(nome, municipio.nome) &&
                Objects.equals(codigoIbge, municipio.codigoIbge) &&
                Objects.equals(estado, municipio.estado);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), nome, codigoIbge, estado);
    }
}

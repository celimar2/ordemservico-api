package br.net.gradual.core.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "PasswordResetToken",
        indexes = {
        // @Index(name = "idx_token", columnList = "token"),
                   @Index(name = "idx_user", columnList = "user_id")},
        uniqueConstraints = {@UniqueConstraint( name="uk_token", columnNames = {"token"} ) })
public class PasswordResetToken extends AbstractEntity{

    private static final int EXPIRATION = 8;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id", foreignKey =@ForeignKey(name = "fk_user"))
    private User user;

    @Column
    private String token;

    @Column
    private LocalDateTime expiryDate;

    @Column
    private Boolean usedToken;

    public PasswordResetToken() { }

    public PasswordResetToken(String token, User user) {
        this.token = token;
        this.user = user;
        this.expiryDate = LocalDateTime.now().plusHours(EXPIRATION);
        this.usedToken = false;
    }

    public Boolean isExpired() {
        return this.expiryDate.isBefore(LocalDateTime.now());
    }

    public Boolean isUser(User user) {
        return this.user.getId() == user.getId();
    }

    public Boolean isUserId(Long id) {
        return this.user.getId() == id;
    }

//    public static int getEXPIRATION() {
//        return EXPIRATION;
//    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Boolean getUsedToken() {
        return usedToken;
    }

    public void setUsedToken(Boolean usedToken) {
        this.usedToken = usedToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PasswordResetToken that = (PasswordResetToken) o;
        return Objects.equals(token, that.token) &&
                Objects.equals(user, that.user) &&
                Objects.equals(expiryDate, that.expiryDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), token, user, expiryDate);
    }
}

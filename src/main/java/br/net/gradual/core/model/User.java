package br.net.gradual.core.model;

import br.net.gradual.business.model.Colaborador;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "User",
        uniqueConstraints = {@UniqueConstraint( name="UK_User_Username", columnNames = {"username"} ) })
public class User extends AbstractEntity {

    @NotNull
    @Column(length = 50)
    private String username;

    @Column
//    @JsonIgnore
    private String password;

    @Column(length = 100)
    @Email
    private String email;

    @Column(length = 100)
    private String firstName;
    @Column(length = 100)
    private String lastName;

    @NotNull
    @Column(columnDefinition = "boolean DEFAULT 1")
    private Boolean active;
    @NotNull
    @Column(columnDefinition = "boolean DEFAULT 0")
    private Boolean blocked;

    @NotNull
    @Column(columnDefinition = "boolean DEFAULT 0")
    private Boolean changePasswordNextLogin;

    @Column
    private LocalDateTime passwordAltered;

    @Column
    private LocalDateTime lastLogin;

    @Column(columnDefinition = "SMALLINT")
    private int loginAttempts;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "USER_ROLE",
            joinColumns = {@JoinColumn(name = "USER_ID", foreignKey=@ForeignKey(name = "FK_User_Role_User"))},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", foreignKey=@ForeignKey(name = "FK_User_Role_Role")) })
    private List<Role> roles = new ArrayList<>();

    @JsonIgnore
    @OneToOne(cascade = CascadeType.DETACH)
    @JoinTable( name = "Colaborador_User",
            joinColumns = { @JoinColumn(name = "user_id", foreignKey=@ForeignKey(name = "FK_Colaborador_User_User")) },
            inverseJoinColumns = { @JoinColumn(name = "colaborador_id", foreignKey=@ForeignKey(name = "FK_Colaborador_User_Colaborador"))} )
    private Colaborador colaborador;

    public User() {
    }

    public User(@NotNull String username, String password, @NotNull Boolean active) {
        this.username = username;
        this.password = password;
        this.active = active;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + this.getId() + '\'' +
                "username='" + this.username + '\'' +
                ", email='" + this.email + '\'' +
                ", firstName='" + this.firstName + '\'' +
                ", lastName='" + this.lastName + '\'' +
                ", active=" + this.active +
                ", blocked=" + this.blocked +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

//    @JsonIgnore
    public String getPassword() {
        return password;
    }

//    @JsonIgnore
    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public LocalDateTime getPasswordAltered() {
        return passwordAltered;
    }

    public void setPasswordAltered(LocalDateTime passwordAltered) {
        this.passwordAltered = passwordAltered;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public int getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(int loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public List<Role> getRoles() {
        return this.roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getChangePasswordNextLogin() {
        return changePasswordNextLogin;
    }

    public void setChangePasswordNextLogin(Boolean changePasswordNextLogin) {
        this.changePasswordNextLogin = changePasswordNextLogin;
    }

    @JsonIgnore
    public Colaborador getColaborador() {
        return colaborador;
    }

    @JsonIgnore
    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }
}

package br.net.gradual.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

@Entity
@Table(name = "Menu")
public class Menu extends AbstractEntity {

    @Column(length = 100, nullable = false)
    private String name;

    @Column(length = 100, nullable = false)
    private String description;

    @Column(nullable = false)
    private float sequence;

    @Column(columnDefinition = "boolean DEFAULT 1", nullable = false)
    private Boolean ativo;

    @ManyToOne
    @Nullable
    @JoinColumn(name = "parentMenu", referencedColumnName = "id", foreignKey=@ForeignKey(name = "Fk_Menu_ParentMenu"))
    private Menu parentMenu;

    @JsonIgnore
    @ManyToOne
    @Nullable
    @JoinColumn(name = "pageId", referencedColumnName = "id", foreignKey=@ForeignKey(name = "Fk_Menu_Page"))
    private Page page;

    public Menu() {
    }

    public Menu(@NotEmpty String name, @NotEmpty int sequence, Menu parentMenu, Page page) {
        this.name = name;
        this.sequence = sequence;
        this.parentMenu = parentMenu;
        this.page = page;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Menu)) return false;
        if (!super.equals(o)) return false;
        Menu menu = (Menu) o;

        return o == null ? false : this.getName().equals(menu.getName()); // Objects.equals(name, menu.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSequence() {
        return sequence;
    }

    public void setSequence(float sequence) {
        this.sequence = sequence;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentMenuName() {
        return parentMenu == null ? null : parentMenu.getName();
    }

    public Long getParentMenuId() {
        return parentMenu == null ? null : parentMenu.getId();
    }

    @JsonIgnore
    public Menu getParentMenu() {
        return parentMenu;
    }

    @JsonIgnore
    public void setParentMenu(Menu parentMenu) {
        this.parentMenu = parentMenu;
    }

    @JsonIgnore
    public Page getPage() {
        return page;
    }

    @JsonIgnore
    public void setPage(Page page) {
        this.page = page;
    }

    public String getCrud() {
        if (this.page == null) {
            return null;
        }
        return this.page.getCrud();
    }

}

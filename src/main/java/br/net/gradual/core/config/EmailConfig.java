package br.net.gradual.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
//@PropertySource({
//        "classpath:/application.properties"
//        "classpath:/src/main/filters/application-${spring.profiles.active}.properties"
//        "classpath:/${spring.profiles.active}/application-${spring.profiles.active}.properties"
//        "classpath:/application-${spring.profiles.active}.properties"
//        })
public class EmailConfig {

    @Autowired
    Environment env;

    private static final String MAIL_PROTOCOL ="mail.transport.protocol";
    private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    private static final String MAIL_SMTP_STARTTLS = "mail.smtp.starttls.enable";
    private static final String MAIL_DEBUG = "mail.debug";
    private static final String SMTP_PROTOCOL ="smtp";
    private static final String STRING_TRUE ="true";
    private static final String STRING_FALSE ="false";

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.port}")
    private Integer port;

    @Value("${spring.mail.username}")
    private String username;

    @Value("${spring.mail.password}")
    private String password;

//    private String host;
//    private Integer port;
//    private String username;
//    private String password;

//    public EmailConfig() {
//        host = "smtp";
//        port = 587;
//        username ="reset.password@gradual.net.br";
//        password = "gradual.net";
//        host = env.getProperty("spring.mail.host");
////        port = Integer.parseInt(env.getProperty("spring.mail.port"));
////        username = env.getProperty("spring.mail.username");
////        password = env.getProperty("spring.mail.password");
//    }

    @Bean
    public JavaMailSender javaMailService() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost(host);
        javaMailSender.setPort(port);
        javaMailSender.setUsername(username);
        javaMailSender.setPassword(password);
        javaMailSender.setJavaMailProperties(getMailProperties());

        return javaMailSender;
    }

    private Properties getMailProperties() {
        Properties properties = new Properties();
        properties.setProperty(MAIL_PROTOCOL, SMTP_PROTOCOL);
        properties.setProperty(MAIL_SMTP_AUTH, STRING_TRUE);
        properties.setProperty(MAIL_SMTP_STARTTLS, STRING_TRUE);
        properties.setProperty(MAIL_DEBUG, STRING_FALSE);
        return properties;
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}

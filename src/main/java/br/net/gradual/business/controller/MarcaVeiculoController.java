package br.net.gradual.business.controller;

import br.net.gradual.business.model.MarcaVeiculo;
import br.net.gradual.business.repository.MarcaVeiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/marca-veiculos")
public class MarcaVeiculoController {

    private final MarcaVeiculoRepository marcaVeiculoDAO;

    @Autowired
    public MarcaVeiculoController(MarcaVeiculoRepository marcaVeiculoDAO) {
        this.marcaVeiculoDAO = marcaVeiculoDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(marcaVeiculoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(marcaVeiculoDAO.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody MarcaVeiculo marcaVeiculo) {
        return new ResponseEntity<>(marcaVeiculoDAO.save(marcaVeiculo), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody MarcaVeiculo marcaVeiculo) {
        return new ResponseEntity<>(marcaVeiculoDAO.save(marcaVeiculo), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        marcaVeiculoDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

package br.net.gradual.business.controller;

import br.net.gradual.business.model.AtividadeEconomica;
import br.net.gradual.business.repository.AtividadeEconomicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/atividades-economicas")
public class AtividadeEconomicaController {

    private final AtividadeEconomicaRepository atividadeEconomicaDAO;

    @Autowired
    public AtividadeEconomicaController(AtividadeEconomicaRepository atividadeEconomicaDAO) {
        this.atividadeEconomicaDAO = atividadeEconomicaDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(atividadeEconomicaDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/ativos")
    public ResponseEntity<?> listAtivos() {
        return new ResponseEntity<>(atividadeEconomicaDAO.findByAtivo(true), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(atividadeEconomicaDAO.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody AtividadeEconomica atividadeEconomica) {
        return new ResponseEntity<>(atividadeEconomicaDAO.save(atividadeEconomica), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody AtividadeEconomica atividadeEconomica) {
        return new ResponseEntity<>(atividadeEconomicaDAO.save(atividadeEconomica), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        atividadeEconomicaDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

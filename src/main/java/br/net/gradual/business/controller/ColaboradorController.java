package br.net.gradual.business.controller;

import br.net.gradual.business.model.Colaborador;
import br.net.gradual.business.model.CpfCnpjRequest;
import br.net.gradual.business.repository.ColaboradorRepository;
import br.net.gradual.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/colaboradores")
public class ColaboradorController {

    private final ColaboradorRepository colaboradorDAO;

    @Autowired
    private UserService userService;


    @Autowired
    public ColaboradorController(ColaboradorRepository colaboradorDAO) {
        this.colaboradorDAO = colaboradorDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(colaboradorDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(colaboradorDAO.findById(id).get(), HttpStatus.OK);
    }

//    @GetMapping(path="/users-no-colab")
//    public ResponseEntity<?> getById() {
//
//        List<User> users = userService.findAll();
//        users.stream().filter()
//        return new ResponseEntity<>(colaboradorDAO.findById(id).get(), HttpStatus.OK);
//    }

    @PutMapping(path="/cpf")
    public ResponseEntity<?> getByCpfCnpj(@RequestBody CpfCnpjRequest cpf) {
        return new ResponseEntity<>(colaboradorDAO.findByCpf(cpf.getValue()), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@RequestBody Colaborador colaborador) {
        return new ResponseEntity<>(colaboradorDAO.save(colaborador), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Colaborador colaborador) {
        return new ResponseEntity<>(colaboradorDAO.save(colaborador), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        colaboradorDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

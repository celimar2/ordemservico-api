package br.net.gradual.business.controller;

import br.net.gradual.business.model.OrdemServico;
import br.net.gradual.business.model.OrdemServicoListagem;
import br.net.gradual.business.model.OsListByFornecedor;
import br.net.gradual.business.model.TinyOrdemServico;
import br.net.gradual.business.repository.FornecedorRepository;
import br.net.gradual.business.repository.OrdemServicoRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/ordens-servico")
public class OrdemServicoController {

    private final OrdemServicoRepository ordemServicoDAO;
    private final FornecedorRepository fornecedorDO;

    @Autowired
    public OrdemServicoController(OrdemServicoRepository ordemServicoDAO, FornecedorRepository fornecedorDO) {
        this.ordemServicoDAO = ordemServicoDAO;
        this.fornecedorDO = fornecedorDO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(ordemServicoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/listagem")
    public ResponseEntity<?> getListagem() {
        List<OrdemServicoListagem> listagem = new ArrayList<>();
        ordemServicoDAO.findAll().forEach( os -> {
            listagem.add( new OrdemServicoListagem(
                    os.getId(),
                    os.getVeiculo().getPlaca() +" " + os.getVeiculo().getModelo() +" " + os.getVeiculo().getCor(),
                    os.getFornecedor().getApelidoNomeFantasia(),
                    os.getSituacaoServico().getNome(),
                    os.getRetiradaDataHora(),
                    os.getDevolucaoDataHora()
            ));
        });
        return new ResponseEntity<>(listagem, HttpStatus.OK);
    }

    //    @GetMapping(path="/byFornecedor/{incluiOsFechadas}")
//    public ResponseEntity<?> listAllbyFornecedor(@PathVariable("incluiOsFechadas") Boolean fechadas) {
    @GetMapping(path="/byFornecedor")
    public ResponseEntity<?> listAllbyFornecedor() {

        List<OsListByFornecedor> lista = new ArrayList<>();

        ordemServicoDAO.findAll().forEach( os -> {

            TinyOrdemServico tnos = new TinyOrdemServico(
                    os.getId(),
                    os.getDescricao(),
                    os.getAbertura(),
                    os.getDevolucaoPrevisao(),
                    os.getDevolucaoDataHora(),
                    os.getNaturezaServico().getNome(),
                    os.getSituacaoServico().getNome(),
                    os.getVeiculo().getId(),
                    os.getVeiculo().getPlaca(),
                    os.getVeiculo().getMarca().getNome(),
                    os.getVeiculo().getModelo(),
                    os.getVeiculo().getCor()
            );

            OsListByFornecedor frn =  new OsListByFornecedor(
                    os.getFornecedor().getId(),
                    os.getFornecedor().getApelidoNomeFantasia()
            );

            if ( ! lista.contains(frn) ) {
                lista.add(frn);
            }

            lista.  get( lista.indexOf(frn) ).getOrdens().add(tnos);
        });

        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(ordemServicoDAO.findById(id).get(), HttpStatus.OK);
    }

//    @PostMapping
//    @Transactional(rollbackFor = Exception.class)
//    public void ResponseEntity<?> save(@RequestBody String payload) {
//        System.out.println(payload);
//        OrdemServico ordemServico = new ObjectMapper().readValue(payload, OrdemServico.class);
////        objectMapper.writeValueAsString(link);
//        return new ResponseEntity<>(ordemServicoDAO.save(ordemServico), HttpStatus.CREATED);
//    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody OrdemServico ordemServico) throws JsonProcessingException {
//        System.out.println("##### POST #######################################################################");
//        System.out.println( new ObjectMapper().writeValueAsString(ordemServico) );
//        System.out.println("############################################################################");
        return new ResponseEntity<>(ordemServicoDAO.save(ordemServico), HttpStatus.CREATED);
    }

//    @PutMapping
//    @Transactional(rollbackFor = Exception.class)
//    public ResponseEntity<?> update(@RequestBody String payload) throws IOException {
//        System.out.println("#### PUT ########################################################################");
//        System.out.println(payload);
//        OrdemServico ordemServico = new ObjectMapper().readValue(payload, OrdemServico.class);
//        System.out.println("############################################################################");
//        return new ResponseEntity<>(ordemServicoDAO.save(ordemServico), HttpStatus.OK);
//    }
    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody OrdemServico ordemServico) {
        return new ResponseEntity<>(ordemServicoDAO.save(ordemServico), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        ordemServicoDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

package br.net.gradual.business.controller;

import br.net.gradual.business.model.CpfCnpjRequest;
import br.net.gradual.business.model.Fornecedor;
import br.net.gradual.business.repository.FornecedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/fornecedores")
public class FornecedorController {
          
    private final FornecedorRepository fornecedorDAO;

    @Autowired
    public FornecedorController(FornecedorRepository fornecedorDAO) {
        this.fornecedorDAO = fornecedorDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(fornecedorDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(fornecedorDAO.findById(id).get(), HttpStatus.OK);
    }

    @PutMapping(path="/cpf-cnpj")
    public ResponseEntity<?> getByCpfCnpj(@RequestBody CpfCnpjRequest cpfCnpj) {
        return new ResponseEntity<>(fornecedorDAO.findByCpfCnpj(cpfCnpj.getValue()), HttpStatus.OK);
    }

    @GetMapping(path="/{id}/atividade-economica")
    public ResponseEntity<?> getAtividadeEconomicaById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(fornecedorDAO.findById(id).get().getAtividadesEconomicas(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@RequestBody Fornecedor fornecedor) {
        return new ResponseEntity<>(fornecedorDAO.save(fornecedor), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Fornecedor fornecedor) {
        return new ResponseEntity<>(fornecedorDAO.save(fornecedor), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        fornecedorDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

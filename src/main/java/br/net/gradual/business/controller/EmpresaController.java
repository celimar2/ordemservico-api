package br.net.gradual.business.controller;

import br.net.gradual.business.model.Empresa;
import br.net.gradual.business.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/empresas")
public class EmpresaController {

    private final EmpresaRepository empresaDAO;

    @Autowired
    public EmpresaController(EmpresaRepository empresaDAO) {
        this.empresaDAO = empresaDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(empresaDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(empresaDAO.findById(id).get(), HttpStatus.OK);
    }

//    @GetMapping(path="/{id}/atividade-economica")
//    public ResponseEntity<?> getAtividadeEconomicaById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(empresaDAO.findById(id).get().getAtividadesEconomicas(), HttpStatus.OK);
//    }
//
//    @GetMapping(path="/{id}/parametro")
//    public ResponseEntity<?> getParametroById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(empresaDAO.findById(id).get().getParametros(), HttpStatus.OK);
//    }
//
//    @GetMapping(path="/{id}/filial")
//    public ResponseEntity<?> getFilialById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(empresaDAO.findById(id).get().getFilialList(), HttpStatus.OK);
//    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody Empresa empresa) {
        return new ResponseEntity<>(empresaDAO.save(empresa), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Empresa empresa) {
        return new ResponseEntity<>(empresaDAO.save(empresa), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        empresaDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

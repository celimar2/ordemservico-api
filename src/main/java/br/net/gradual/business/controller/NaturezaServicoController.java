package br.net.gradual.business.controller;

import br.net.gradual.business.model.NaturezaServico;
import br.net.gradual.business.repository.NaturezaServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/natureza-servicos")
public class NaturezaServicoController {

    private final NaturezaServicoRepository naturezaServicoDAO;

    @Autowired
    public NaturezaServicoController(NaturezaServicoRepository naturezaServicoDAO) {
        this.naturezaServicoDAO = naturezaServicoDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(naturezaServicoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(naturezaServicoDAO.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody NaturezaServico naturezaServico) {
        return new ResponseEntity<>(naturezaServicoDAO.save(naturezaServico), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody NaturezaServico naturezaServico) {
        return new ResponseEntity<>(naturezaServicoDAO.save(naturezaServico), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        naturezaServicoDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

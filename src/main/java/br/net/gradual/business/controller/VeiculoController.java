package br.net.gradual.business.controller;

import br.net.gradual.business.model.Total;
import br.net.gradual.business.model.Veiculo;
import br.net.gradual.business.model.VeiculoListagem;
import br.net.gradual.business.model.VeiculoSituacaoHistorico;
import br.net.gradual.business.model.collections.SituacaoVeiculo;
import br.net.gradual.business.repository.VeiculoSituacaoHistoricoRepository;
import br.net.gradual.business.service.VeiculoService;
import org.apache.lucene.util.ArrayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
@RequestMapping("api/veiculos")
public class VeiculoController {

    //    private final VeiculoRepository veiculoDAO;
    private final VeiculoService veiculoDAO;
    private final VeiculoSituacaoHistoricoRepository vshRepository;

    @Autowired
    public VeiculoController(VeiculoService veiculoDAO, VeiculoSituacaoHistoricoRepository vshRepository) {
        this.veiculoDAO = veiculoDAO;
        this.vshRepository = vshRepository;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(veiculoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/all-status/{data}")
    public ResponseEntity<?> getStatusByData(@PathVariable("data") LocalDate data) {
        Map<String, Long> total = new HashMap<>();
        veiculoDAO.findAll().forEach(veiculo -> {
        });
        return new ResponseEntity<>(veiculoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(veiculoDAO.findById(id), HttpStatus.OK);
    }

    @GetMapping(path = "/listagem-por-situacao")
    public ResponseEntity<?> getReportListagemPorSituacao(
            @RequestParam(name = "from") String from,
            @RequestParam(name = "to") String to,
            @RequestParam(name = "situacoes", required = false, value = "") String[] situacoes
    ) throws Exception {

        List<VeiculoSituacaoHistorico> lista;
        if (situacoes == null) {
            lista = vshRepository.findAllByDataBetween(
                    LocalDateTime.parse(from),
                    LocalDateTime.parse(to));
        } else {
            List<SituacaoVeiculo> situacoesVeiculo = new ArrayList<>();
            for (String s : situacoes) {
                situacoesVeiculo.add(SituacaoVeiculo.fromValor(s));
            }
            lista = vshRepository.findAllByDataBetweenAndSituacaoIsIn(
                    LocalDateTime.parse(from),
                    LocalDateTime.parse(to),
                    situacoesVeiculo);
        }

        Map<Veiculo, VeiculoSituacaoHistorico> listagem = new HashMap<>();

        lista.forEach(v -> {
            String placa = v.getVeiculo().getPlaca();
            if (listagem.containsKey(v.getVeiculo())) {
                if (v.getData().compareTo(listagem.get(v.getVeiculo()).getData()) > 0) {
                    listagem.replace(v.getVeiculo(), v);
                }
            } else {
                listagem.put(v.getVeiculo(), v);
            }
        });
        return new ResponseEntity<>(listagem, HttpStatus.OK);
    }


    @GetMapping(path = "/list")
    public ResponseEntity<?> getReportList() throws Exception {
        return new ResponseEntity<>(veiculoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/listagem")
    public ResponseEntity<?> getReportListagem(
            @RequestParam(name = "from") String from,
            @RequestParam(name = "to") String to,
            @RequestParam(name = "situacoes", required = false, value = "") String[] situacoes
    ) throws Exception {
        List<SituacaoVeiculo> situacoesVeiculo = new ArrayList<>();
        if (situacoes != null) {
            for (String s : situacoes) {
                situacoesVeiculo.add(SituacaoVeiculo.fromValor(s));
            }
        } else {
            situacoesVeiculo.addAll(Arrays.asList(SituacaoVeiculo.values()));
        }
        if (from == null) from = "2010-01-01";
        if (to == null) to = "2030-01-01";
        if (situacoes != null) {
            situacoes = new String[situacoesVeiculo.toArray().length];
            int i = 0;
            for (SituacaoVeiculo situacaoVeiculo : situacoesVeiculo) {
                situacoes[i++] = situacaoVeiculo.getValor();
            }
        }
        List<VeiculoListagem> listagem = new ArrayList<>();
        LocalDateTime ldtFrom = LocalDateTime.parse(from + "T00:00");
        LocalDateTime ldtTo = LocalDateTime.parse(to + "T00:00");

        veiculoDAO.findAll().stream()
                .filter(veiculo -> veiculo.getDataSituacao().compareTo(ldtFrom.minusHours(12)) >= 0)
                .filter(veiculo -> veiculo.getDataSituacao().compareTo(ldtTo.minusHours(12).plusDays(1)) <= 0)
                .filter(veiculo -> situacoesVeiculo.contains(veiculo.getSituacao()))
                .forEach(v ->
                        listagem.add(new VeiculoListagem(
                                        v.getId(),
                                        v.getPlaca(),
                                        v.getMarca().getNome(),
                                        v.getModelo(),
                                        v.getCor(),
                                        v.getSituacao().name().replace("_", " "),
                                        v.getDataSituacao(),
                                        v.getEntrada(),
                                        v.getSaida()
                                )
                        )
                );
        return new ResponseEntity<>(listagem, HttpStatus.OK);
    }

    @GetMapping(path = "/totais")
    public ResponseEntity<?> getReportListagem(
            @RequestParam(name = "from") String from,
            @RequestParam(name = "to") String to
    ) throws Exception {

        if (from == null) from = "2010-01-01";
        if (to == null) to = "2030-01-01";
        Map<String, Integer> totais = new HashMap<>();
        List<Total> totaisList = new ArrayList<>();
        LocalDateTime ldtFrom = LocalDateTime.parse(from + "T00:00");
        LocalDateTime ldtTo = LocalDateTime.parse(to + "T00:00");
        veiculoDAO.findAll().stream()
                .filter(veiculo -> veiculo.getDataSituacao().compareTo(ldtFrom.minusHours(12)) >= 0)
                .filter(veiculo -> veiculo.getDataSituacao().compareTo(ldtTo.minusHours(12).plusDays(1)) <= 0)
                .forEach(veiculo -> {
                    String sv = veiculo.getSituacao().name().replace("_", " ");
                    if (totais.containsKey(sv)) {
                        totais.replace(sv, totais.get(sv) + 1);
                    } else {
                        totais.put(sv, 1);
                    }
                });

        totaisList.clear();
        totais.keySet().forEach(s ->
                totaisList.add( new Total(s, totais.get(s) ) )
        );

        System.out.println(totais.toString());
        System.out.println(totaisList.toString());
        return new ResponseEntity<>(totaisList, HttpStatus.OK);
    }

    @GetMapping(path = "/placa/{placa}")
    public ResponseEntity<?> getVeiculoById(@PathVariable("placa") String placa) {
        return new ResponseEntity<>(veiculoDAO.findByPlaca(placa), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody Veiculo veiculo) {
        return new ResponseEntity<>(veiculoDAO.save(veiculo), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Veiculo veiculo) {
        return new ResponseEntity<>(veiculoDAO.save(veiculo), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        veiculoDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public static void main(String[] args) {
//        System.out.println(LocalDateTime.now().toString());
    }

}

package br.net.gradual.business.controller;

import br.net.gradual.business.model.Filial;
import br.net.gradual.business.repository.FilialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/filiais")
public class FilialController {

    private final FilialRepository filialDAO;

    @Autowired
    public FilialController(FilialRepository filialDAO) {
        this.filialDAO = filialDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(filialDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(filialDAO.findById(id).get(), HttpStatus.OK);
    }

//    @GetMapping(path="/{id}/atividade-economica")
//    public ResponseEntity<?> getAtividadeEconomicaById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(filialDAO.findById(id).get().getAtividadesEconomicas(), HttpStatus.OK);
//    }
//
//    @GetMapping(path="/{id}/endereco")
//    public ResponseEntity<?> getEnderecoById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(filialDAO.findById(id).get().getEnderecos(), HttpStatus.OK);
//    }
//
//    @GetMapping(path="/{id}/anotacao")
//    public ResponseEntity<?> getAnotacaoById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(filialDAO.findById(id).get().getAnotacoes(), HttpStatus.OK);
//    }
//
//    @GetMapping(path="/{id}/parametro")
//    public ResponseEntity<?> getParametroById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(filialDAO.findById(id).get().getParametros(), HttpStatus.OK);
//    }

    @GetMapping(path="/{id}/filial")
    public ResponseEntity<?> getFilialById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(filialDAO.findById(id).get().getEmpresa(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody Filial filial) {
        return new ResponseEntity<>(filialDAO.save(filial), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody Filial filial) {
        return new ResponseEntity<>(filialDAO.save(filial), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        filialDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

package br.net.gradual.business.controller;

import br.net.gradual.business.model.SituacaoServico;
import br.net.gradual.business.repository.SituacaoServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/situacao-servicos")
public class SituacaoServicoController {

    private final SituacaoServicoRepository situacaoServicoDAO;

    @Autowired
    public SituacaoServicoController(SituacaoServicoRepository situacaoServicoDAO) {
        this.situacaoServicoDAO = situacaoServicoDAO;
    }

    @GetMapping
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(situacaoServicoDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(situacaoServicoDAO.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> save(@Validated @RequestBody SituacaoServico situacaoServico) {
        return new ResponseEntity<>(situacaoServicoDAO.save(situacaoServico), HttpStatus.CREATED);
    }

    @PutMapping
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> update(@Validated @RequestBody SituacaoServico situacaoServico) {
        return new ResponseEntity<>(situacaoServicoDAO.save(situacaoServico), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> delete(@Validated @PathVariable("id") Long id) {
        situacaoServicoDAO.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

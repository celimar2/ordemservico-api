package br.net.gradual.business.model;

import br.net.gradual.core.model.AbstractEntity;
import br.net.gradual.core.model.Estado;
import br.net.gradual.core.model.Municipio;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Empresa",
        uniqueConstraints = {
                @UniqueConstraint( columnNames = {"cnpj"}, name="UK_Empresa_Cnpj"),
                @UniqueConstraint( columnNames = {"razaoSocial"}, name="UK_Empresa_razaoSocial"),
                @UniqueConstraint( columnNames = {"nomeFantasia"}, name="UK_Empresa_nomeFantasia")
        })
public class Empresa extends AbstractEntity {
    @NotEmpty
    @Column(length = 20)
    private String cnpj;

    @Column(length = 200)
    private String razaoSocial;

    @Column(length = 200)
    private String nomeFantasia;

    @Column(length = 20)
    private String inscricaoEstadual;

    @Column(length = 20)
    private String inscricaoMunicipal;

    @Temporal(TemporalType.DATE)
    private Date abertura;

    @JsonIgnore
    @Column(length = 1000)
    private String chaveSistema;

    @Email
    @Column(length = 100)
    @Email
    private String email;

    @NotNull
    private Boolean ativa;

    @Column(columnDefinition = "TEXT(2000)", length = 2000)
    private String notas;

    @Column(length = 10)
    private String cep;

    @NotEmpty
    @Column(length = 100)
    private String logradouro;

    @Column(length = 100)
    private String complemento;

    @Column(length = 80)
    private String bairro;

    @ManyToOne
    @JoinColumn(name = "uf", referencedColumnName = "uf", foreignKey = @ForeignKey(name = "FK_Empresa_Estado"))
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "municipioId", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Empresa_Municipio"))
    private Municipio municipio;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "Empresa_Telefone",
            joinColumns = {@JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "FK_Empresa_Telefone_Empresa"))},
            inverseJoinColumns = {@JoinColumn(name = "telefone_id", foreignKey = @ForeignKey(name = "FK_Empresa_Telefone_Telefone"))})
    private List<Telefone> telefones = new ArrayList<>();

    public Empresa() {
    }

    public Empresa(@NotEmpty String cnpj, String razaoSocial) {
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getInscricaoMunicipal() {
        return inscricaoMunicipal;
    }

    public void setInscricaoMunicipal(String inscricaoMunicipal) {
        this.inscricaoMunicipal = inscricaoMunicipal;
    }

    public Date getAbertura() {
        return abertura;
    }

    public void setAbertura(Date abertura) {
        this.abertura = abertura;
    }

    public String getChaveSistema() {
        return chaveSistema;
    }

    public void setChaveSistema(String chaveSistema) {
        this.chaveSistema = chaveSistema;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Empresa empresa = (Empresa) o;
        return Objects.equals(cnpj, empresa.cnpj) &&
                Objects.equals(razaoSocial, empresa.razaoSocial);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), cnpj, razaoSocial);
    }
}
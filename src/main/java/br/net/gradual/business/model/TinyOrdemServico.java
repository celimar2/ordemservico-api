package br.net.gradual.business.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class TinyOrdemServico {

    private Long id;
    private String descricao;
    private LocalDate abertura;
    private LocalDateTime previsao;
    private LocalDateTime devolucao;
    private String natureza;
    private String situacao;
    private Long veiculoId;
    private String placa;
    private String marca;
    private String modelo;
    private String cor;

    public TinyOrdemServico(Long id, String descricao, LocalDate abertura, LocalDateTime previsao,
                            LocalDateTime devolucao, String natureza, String situacao, Long veiculoId,
                            String placa, String marca, String modelo, String cor) {
        this.id = id;
        this.descricao = descricao;
        this.abertura = abertura;
        this.previsao = previsao;
        this.devolucao = devolucao;
        this.natureza = natureza;
        this.situacao = situacao;
        this.veiculoId = veiculoId;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.cor = cor;
    }

    public TinyOrdemServico() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getAbertura() {
        return abertura;
    }

    public void setAbertura(LocalDate abertura) {
        this.abertura = abertura;
    }

    public LocalDateTime getPrevisao() {
        return previsao;
    }

    public void setPrevisao(LocalDateTime devolucaoPrevisao) {
        this.previsao = devolucaoPrevisao;
    }

    public LocalDateTime getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(LocalDateTime devolucao) {
        this.devolucao = devolucao;
    }

    public String getNatureza() {
        return natureza;
    }

    public void setNatureza(String natureza) {
        this.natureza = natureza;
    }

    public Long getVeiculoId() {
        return veiculoId;
    }

    public void setVeiculoId(Long veiculoId) {
        this.veiculoId = veiculoId;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
}

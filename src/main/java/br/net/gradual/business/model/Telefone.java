package br.net.gradual.business.model;

import br.net.gradual.business.model.collections.TipoTelefone;
import br.net.gradual.core.model.AbstractEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Telefone")
public class Telefone extends AbstractEntity {

    @Column(length = 20)
    private String numero;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = " varchar(20)")
    private TipoTelefone tipo;

    public Telefone() {
    }

    public Telefone(String numero, TipoTelefone tipo) {
        this.numero = numero;
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Telefone telefone = (Telefone) o;
        return Objects.equals(numero, telefone.numero) &&
                tipo == telefone.tipo;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), numero, tipo);
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public TipoTelefone getTipo() {
        return tipo;
    }

    public void setTipo(TipoTelefone tipo) {
        this.tipo = tipo;
    }
}

package br.net.gradual.business.model.collections;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum TipoJuridico implements Serializable {

    PESSOA_FISICA ("PF"),
    PESSOA_JURIDICA ("PJ");

    private final String valor;

    TipoJuridico(String valor) {
        this.valor = valor;
    }

    @JsonValue
    public String getValor(){
        return valor;
    }

    public static TipoJuridico fromValor(String valor) {
        for (TipoJuridico tj : TipoJuridico.values()) {
            if (tj.getValor().equals(valor)) {
                return tj;
            }
        }
        throw new IllegalArgumentException("Valor [" + valor+ "] não suportado.");
    }


}

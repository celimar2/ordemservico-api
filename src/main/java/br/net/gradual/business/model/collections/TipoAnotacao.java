package br.net.gradual.business.model.collections;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoAnotacao {

    EMAIL ("EML"),
    TELEFONE ("TEL"),
    OUTRO("OUT");

    private final String  valor;

    TipoAnotacao(String valor) {
        this.valor = valor;
    }

    @JsonValue
    public String getValor(){
        return valor;
    }

    public static TipoAnotacao fromValor(String valor) {
        for (TipoAnotacao ta : TipoAnotacao.values()) {
            if (ta.getValor().equals(valor)) {
                return ta;
            }
        }
        throw new IllegalArgumentException("Valor [" + valor+ "] não suportado.");
    }

}

package br.net.gradual.business.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OsListByFornecedor {

    private Long id;
    private String apelido;
    private List<TinyOrdemServico> ordens = new ArrayList<>();

    public OsListByFornecedor(Long id, String apelido) {
        this.id = id;
        this.apelido = apelido;
    }

    public OsListByFornecedor() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OsListByFornecedor)) return false;
        OsListByFornecedor that = (OsListByFornecedor) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public List<TinyOrdemServico> getOrdens() {
        return ordens;
    }

    public void setOrdens(List<TinyOrdemServico> ordens) {
        this.ordens = ordens;
    }
}
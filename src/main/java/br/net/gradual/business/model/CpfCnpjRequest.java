package br.net.gradual.business.model;

public class CpfCnpjRequest {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CpfCnpjRequest() {
    }

    public CpfCnpjRequest(String value) {
        this.value = value;
    }
}


package br.net.gradual.business.model;

import br.net.gradual.core.model.AbstractEntity;
import br.net.gradual.core.model.Estado;
import br.net.gradual.core.model.Municipio;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Filial",
        uniqueConstraints = {
                @UniqueConstraint( columnNames = {"cnpj"}, name="UK_Filial_Cnpj"),
                @UniqueConstraint( columnNames = {"razaoSocial"}, name="UK_Filial_razaoSocial"),
                @UniqueConstraint( columnNames = {"nomeFantasia"}, name="UK_Filial_nomeFantasia")
        })
public class Filial extends AbstractEntity {
    @NotEmpty
    @Column(length = 20)
    private String cnpj;

    @Column(length = 150)
    private String razaoSocial;

    @Column(length = 150)
    private String nomeFantasia;

    @Column(length = 20)
    private String inscricaoEstadual;

    @Column(length = 20)
    private String inscricaoMunicipal;

    @Temporal(TemporalType.DATE)
    private Date abertura;

    @JsonIgnore
    @Column(length = 1000)
    private String chaveSistema;

    @NotNull
    private Boolean ativa;

    @Column(length = 8)
    private String cep;

    @Column(columnDefinition = "TEXT(2000)", length = 2000)
    private String notas;

    @NotEmpty
    @Column(length = 100)
    private String logradouro;

    @Column(length = 100)
    private String complemento;

    @Column(length = 80)
    private String bairro;

    @ManyToOne
    @JoinColumn(name = "uf", referencedColumnName = "uf", foreignKey = @ForeignKey(name = "FK_Filial_Estado"))
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "municipioId", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Filial_Municipio"))
    private Municipio municipio;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable( name = "Filial_Telefone",
//            uniqueConstraints=@UniqueConstraint(name="UK_Filial_Atividade_Economica", columnNames={"fornecedor_id", "atividade_Economica_id"}),
            joinColumns = { @JoinColumn(name = "filial_id", foreignKey=@ForeignKey(name = "FK_Filial_Telefone_Filial")) },
            inverseJoinColumns = { @JoinColumn(name = "telefone_id", foreignKey=@ForeignKey(name = "FK_Filial_Telefone_Telefone"))} )
    private List<Telefone> telefones = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinTable( name = "Filial_Atividade_Economica",
//            joinColumns = { @JoinColumn(name = "filial_id", foreignKey=@ForeignKey(name = "FK_Filial_Atividade_Economica_Filial")) },
//            inverseJoinColumns = { @JoinColumn(name = "atividade_economica_id", foreignKey=@ForeignKey(name = "FK_Filial_Atividade_Economica_Atividade_Economica"))} )
//    private Set<AtividadeEconomica> atividadesEconomicas = new HashSet<>();

//    @ManyToMany(cascade = { CascadeType.ALL })
//    @JoinTable( name = "Filial_Parametro",
//            joinColumns = { @JoinColumn(name = "filial_id", foreignKey=@ForeignKey(name = "FK_Filial_Parametro_Filial")) },
//            inverseJoinColumns = { @JoinColumn(name = "parametro_id", foreignKey=@ForeignKey(name = "FK_Filial_Parametro_Parametro"))} )
//    private Set<Parametro> parametros= new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "empresaId", referencedColumnName = "id",
            foreignKey=@ForeignKey(name = "Fk_Filial_Empresa"))
    private Empresa empresa;

    public Filial() {
    }

    public Filial(@NotEmpty String cnpj, String razaoSocial) {
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getInscricaoMunicipal() {
        return inscricaoMunicipal;
    }

    public void setInscricaoMunicipal(String inscricaoMunicipal) {
        this.inscricaoMunicipal = inscricaoMunicipal;
    }

    public Date getAbertura() {
        return abertura;
    }

    public void setAbertura(Date abertura) {
        this.abertura = abertura;
    }

    public String getChaveSistema() {
        return chaveSistema;
    }

    public void setChaveSistema(String chaveSistema) {
        this.chaveSistema = chaveSistema;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

//    public Set<AtividadeEconomica> getAtividadesEconomicas() {
//        return atividadesEconomicas;
//    }
//
//    public void setAtividadesEconomicas(Set<AtividadeEconomica> atividadesEconomicas) {
//        this.atividadesEconomicas = atividadesEconomicas;
//    }
//
//    public Set<Parametro> getParametros() {
//        return parametros;
//    }
//
//    public void setParametros(Set<Parametro> parametros) {
//        this.parametros = parametros;
//    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @JsonIgnore
    public Empresa getEmpresa() {
        return empresa;
    }

    public Long getEmpresaId() {
        if (this.getEmpresa() != null)
            return this.getEmpresa().getId();
        return null;
    }

    public String getEmpresaNomeFantasia() {
        if (this.getEmpresa() != null)
            return this.getEmpresa().getNomeFantasia();
        return null;
    }

    public String getEmpresaNomeRazaoSocial() {
        if (this.getEmpresa() != null)
            return this.getEmpresa().getRazaoSocial();
        return null;
    }

    public String getEmpresaCnpj() {
        if (this.getEmpresa() != null)
            return this.getEmpresa().getCnpj();
        return null;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }
}

package br.net.gradual.business.model;

public class Total {

    private String key;

    private Integer quantidade;

    public Total(String key, Integer quantidade) {
        this.key = key;
        this.quantidade = quantidade;
    }

    public Total() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}

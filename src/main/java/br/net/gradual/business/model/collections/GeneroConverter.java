package br.net.gradual.business.model.collections;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GeneroConverter  implements AttributeConverter<Genero, String> {

    @Override
    public String convertToDatabaseColumn(Genero genero) {
        return genero.getValor();
    }

    @Override
    public Genero convertToEntityAttribute(String dbData) {
        return Genero.fromValor(dbData);
    }

}
package br.net.gradual.business.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "OrdemServicoOcorrencia")
public class OrdemServicoOcorrencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "Bigint(12) NOT NULL")
    private Long id;

    @Column
    private LocalDateTime data;

    @Column(length = 2000)
    private String observacao;

    @Column
    private BigDecimal valor;

    @JsonIgnore
    @ManyToOne
            (cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "OrdemServicoId", referencedColumnName = "id", foreignKey=@ForeignKey(name = "Fk_OrdemServico") )
    private OrdemServico ordemServico;

    public OrdemServicoOcorrencia() {
    }

    public OrdemServicoOcorrencia(LocalDateTime data, String observacao, BigDecimal valor) {
        this.data = data;
        this.observacao = observacao;
        this.valor = valor;
    }

    public OrdemServicoOcorrencia(LocalDateTime data, String observacao, BigDecimal valor, OrdemServico ordemServico) {
        this.data = data;
        this.observacao = observacao;
        this.valor = valor;
        this.ordemServico = ordemServico;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @JsonIgnore
    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    @JsonIgnore
    public void setOrdemServico(OrdemServico ordemServico) {
        if (this.ordemServico != null) {
            this.ordemServico.getOcorrencias().remove(this);
        }
        if (ordemServico != null) {
            if (ordemServico.getOcorrencias().indexOf(this) == -1) {
                ordemServico.getOcorrencias().add(this);
            }
        }
        this.ordemServico = ordemServico;
    }


}

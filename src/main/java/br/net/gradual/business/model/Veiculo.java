package br.net.gradual.business.model;

import br.net.gradual.business.model.collections.SituacaoVeiculo;
import br.net.gradual.core.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Veiculo",indexes = {@Index(name = "idx_placa", columnList = "placa") } )
public class Veiculo extends AbstractEntity {

    @Column(length = 10)
    private String placa;

    @ManyToOne
    @JoinColumn(name = "marcaId", referencedColumnName = "id",
            foreignKey=@ForeignKey(name = "Fk_Veiculo_MarcaVeiculo") )
    private MarcaVeiculo marca;

    @Column(length = 50)
    private String modelo;

    @Column(length = 20)
    private String cor;

    @Column(length = 4, columnDefinition = ("smallint(4) null"))
    private Integer anoFabricacao;

    @Column(length = 4, columnDefinition = ("smallint(4) null"))
    private Integer anoModelo;

    @Column(length = 100)
    private String origem;

    @Column
    private LocalDateTime entrada;

    @Column
    private LocalDateTime saida;

    @Column(columnDefinition = "varchar(15)")
    @Enumerated(EnumType.STRING)
    private SituacaoVeiculo situacao;

    @Column
    private LocalDateTime dataSituacao;

    @Column(columnDefinition = "TEXT(2000)", length = 2000)
    private String notas;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "veiculo", orphanRemoval = true)
    private List<OrdemServico> ordemServicos = new ArrayList<>();

    public Veiculo() {
    }

    public Veiculo(@NotEmpty String placa) {
        this.placa = placa;
    }


    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public MarcaVeiculo getMarca() {
        return marca;
    }

    public void setMarca(MarcaVeiculo marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Integer getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(Integer anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public Integer getAnoModelo() {
        return anoModelo;
    }

    public void setAnoModelo(Integer anoModelo) {
        this.anoModelo = anoModelo;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public LocalDateTime getEntrada() {
        return entrada;
    }

    public void setEntrada(LocalDateTime entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getSaida() {
        return saida;
    }

    public void setSaida(LocalDateTime saida) {
        this.saida = saida;
    }

    public SituacaoVeiculo getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoVeiculo situacao) {
        this.situacao = situacao;
    }

    public LocalDateTime getDataSituacao() {
        return dataSituacao;
    }

    public void setDataSituacao(LocalDateTime dataSituacao) {
        this.dataSituacao = dataSituacao;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    @JsonIgnore
    public List<OrdemServico> getOrdemServicos() {
        return ordemServicos;
    }

    @JsonIgnore
    public void setOrdemServicos(List<OrdemServico> ordemServicos) {
        this.ordemServicos = ordemServicos;
    }

    @Override
    public String toString() {
        return "\n Veiculo{" +
                "\n  placa='" + placa + '\'' +
                ",\n  marca=" + marca +
                ",\n  modelo='" + modelo + '\'' +
                ",\n  cor='" + cor + '\'' +
                ",\n  anoFabricacao=" + anoFabricacao +
                ",\n  anoModelo=" + anoModelo +
                ",\n  origem='" + origem + '\'' +
                ",\n  entrada=" + entrada +
                ",\n  saida=" + saida +
                ",\n  situacao=" + situacao +
                ",\n  dataSituacao=" + dataSituacao +
                ",\n  notas='" + notas + '\'' +
                " \n }";
    }
}

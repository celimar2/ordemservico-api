package br.net.gradual.business.model.collections;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum SimNao implements Serializable {

    SIM("SIM"),
    NAO("NAO");

    private final String valor;

    SimNao(String valor) {
        this.valor = valor;
    }

    @JsonValue
    public String getValor() {
        return valor;
    }

    public static SimNao fromValor(String valor) {
        for (SimNao sn : SimNao.values()) {
            if (sn.getValor().equals(valor)) {
                return sn;
            }
        }
        throw new IllegalArgumentException("Valor [" + valor+ "] não suportado.");
    }
}

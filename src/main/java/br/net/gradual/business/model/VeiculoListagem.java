package br.net.gradual.business.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class VeiculoListagem implements Serializable {
    private Long id;
    private String placa;
    private String marca;
    private String modelo;
    private String cor;
    private String situacao;
    private LocalDateTime dataSituacao;
    private LocalDateTime entrada;
    private LocalDateTime saida;

    public VeiculoListagem(Long id, String placa, String marca, String modelo, String cor, String situacao, LocalDateTime dataSituacao, LocalDateTime entrada, LocalDateTime saida) {
        this.id = id;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.cor = cor;
        this.situacao = situacao;
        this.dataSituacao = dataSituacao;
        this.entrada = entrada;
        this.saida = saida;
    }

    public Long getId() {
        return id;
    }

    public String getPlaca() {
        return placa;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getCor() {
        return cor;
    }

    public String getSituacao() {
        return situacao;
    }

    public LocalDateTime getEntrada() {
        return entrada;
    }

    public LocalDateTime getSaida() {
        return saida;
    }

    public LocalDateTime getDataSituacao() {
        return dataSituacao;
    }
}

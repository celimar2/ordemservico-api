package br.net.gradual.business.model;

import br.net.gradual.business.model.collections.SituacaoVeiculo;
import br.net.gradual.core.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "veiculo_situacao_historico")
public class VeiculoSituacaoHistorico extends AbstractEntity {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "veiculoId", referencedColumnName = "id",
            foreignKey=@ForeignKey(name = "fk_veiculo_situacao_historico_veiculo") )
    private Veiculo veiculo;

    @Column
    private LocalDateTime data;

    @Column(columnDefinition = "varchar(15)")
    @Enumerated(EnumType.STRING)
    private SituacaoVeiculo situacao;

    public VeiculoSituacaoHistorico() {
    }

    public VeiculoSituacaoHistorico(Veiculo veiculo, LocalDateTime data, SituacaoVeiculo situacao) {
        this.veiculo = veiculo;
        this.data = data;
        this.situacao = situacao;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public SituacaoVeiculo getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoVeiculo situacao) {
        this.situacao = situacao;
    }
}

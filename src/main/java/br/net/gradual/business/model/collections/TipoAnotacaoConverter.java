package br.net.gradual.business.model.collections;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TipoAnotacaoConverter implements AttributeConverter<TipoAnotacao , String> {

    @Override
    public String convertToDatabaseColumn(TipoAnotacao  genero) {
        return genero.getValor();
    }

    @Override
    public TipoAnotacao  convertToEntityAttribute(String dbData) {
        return TipoAnotacao .fromValor(dbData);
    }

}
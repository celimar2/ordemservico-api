package br.net.gradual.business.model;

import br.net.gradual.core.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "SituacaoServico",
        uniqueConstraints={@UniqueConstraint(name="UK_SituacaoServico_nome", columnNames="nome")})
public class SituacaoServico extends AbstractEntity {

    @NotEmpty
    @Column(length = 20)
    private String nome;

    @NotNull
    private int sequencia;

    @Column(length = 255)
    private String descricao;

    @Column(columnDefinition = "boolean DEFAULT 1")
    private boolean ativo;

    public SituacaoServico() {
    }

    public SituacaoServico(@NotEmpty String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase();
    }

    public Integer getSequencia() {
        return sequencia;
    }

    public void setSequencia(Integer sequencia) {
        this.sequencia = sequencia;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setSequencia(int sequencia) {
        this.sequencia = sequencia;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SituacaoServico that = (SituacaoServico) o;
        return Objects.equals(nome, that.nome);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), nome);
    }
}

package br.net.gradual.business.model;

import br.net.gradual.core.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

@Entity
@Table(name = "MarcaVeiculo", uniqueConstraints={
        @UniqueConstraint(name="UK_MarcaVeiculo_Nome", columnNames="nome"),
        @UniqueConstraint(name="UK_MarcaVeiculo_Sigla", columnNames="sigla")})
public class MarcaVeiculo extends AbstractEntity {

    @NotEmpty
    @Column(length = 100)
    private String nome;

    @NotEmpty
    @Column(length = 10)
    private String sigla;

    public MarcaVeiculo() {
    }

    public MarcaVeiculo(@NotEmpty String nome, @NotEmpty String sigla) {
        this.setNome(nome);
        this.setSigla(sigla);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase();
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla.toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MarcaVeiculo that = (MarcaVeiculo) o;
        return Objects.equals(nome, that.nome) &&
                Objects.equals(sigla, that.sigla);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), nome, sigla);
    }
}

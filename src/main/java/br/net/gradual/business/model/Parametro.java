package br.net.gradual.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "Parametro")
public class Parametro {
    @Id
    @Column(length = 100)
    private String chave;

    @NotNull
    @Column(length = 255)
    private String valor;

    @Column(length = 150)
    private String observacao;

    public Parametro() {
    }

    public Parametro(String chave, String valor) {
        this.chave = chave;
        this.valor = valor;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parametro parameter = (Parametro) o;
        return Objects.equals(chave, parameter.chave);
    }

    @Override
    public int hashCode() {

        return Objects.hash(chave);
    }
}


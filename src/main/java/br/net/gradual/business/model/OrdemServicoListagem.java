package br.net.gradual.business.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class OrdemServicoListagem implements Serializable {

    private Long id;
    private String veiculo;
    private String fornecedor;
    private String situacao;
    private LocalDateTime retirada;
    private LocalDateTime devlucao;

    public OrdemServicoListagem(Long id, String veiculo, String fornecedor, String situacao, LocalDateTime retirada, LocalDateTime devlucao) {
        this.id = id;
        this.veiculo = veiculo;
        this.fornecedor = fornecedor;
        this.situacao = situacao;
        this.retirada = retirada;
        this.devlucao = devlucao;
    }

    public Long getId() {
        return id;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public String getSituacao() {
        return situacao;
    }

    public LocalDateTime getRetirada() {
        return retirada;
    }

    public LocalDateTime getDevlucao() {
        return devlucao;
    }
}

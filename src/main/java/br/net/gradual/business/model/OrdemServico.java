package br.net.gradual.business.model;

import br.net.gradual.business.service.VeiculoService;
import br.net.gradual.business.service.impl.VeiculoServiceImpl;
import br.net.gradual.core.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "OrdemServico")
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "id",
//        resolver = EntityIdResolver.class,
//        scope=OrdemServico.class)
public class OrdemServico extends AbstractEntity {

    @Column(length = 1000)
    private String descricao;

    @Column
    private LocalDate abertura;

    @ManyToOne
    @JoinColumn(name = "NaturezaServicoId", referencedColumnName = "id", nullable = true, foreignKey=@ForeignKey(name = "Fk_OrdemServico_NaturezaServico") )
    private NaturezaServico naturezaServico;

//    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "VeiculoId", referencedColumnName = "id", foreignKey=@ForeignKey(name = "Fk_OrdemServico_Veiculo") )
    private Veiculo veiculo;

    @Column
    private LocalDateTime devolucaoPrevisao;

    @Column(length = 100)
    private String numeroOsFornecedor;

//    @ManyToOne
//    @JoinColumn(name = "FilialId", referencedColumnName = "id", foreignKey=@ForeignKey(name = "Fk_OrdemServico_Filial"))
//    private Filial filial;

    @ManyToOne
    @JoinColumn(name = "SituacaoId", referencedColumnName = "id", foreignKey=@ForeignKey(name = "Fk_OrdemServico_SituacaoServico") )
    private SituacaoServico situacaoServico;

    @ManyToOne
    @JoinColumn(name = "FornecedorId", referencedColumnName = "Id", foreignKey=@ForeignKey(name = "Fk_OrdemServico_Fornecedor") )
    private Fornecedor fornecedor;

    @Column
    private LocalDateTime retiradaDataHora;
    @Column
    private BigDecimal retiradaValorPrevisto;
    @Column(length = 1000)
    private String retiradaObservacoes;
    @Column(length = 80)
    private String retiradaResponsavelColaborador;
    @Column(length = 80)
    private String retiradaResponsavelFornecedor;

    @Column
    private LocalDateTime devolucaoDataHora;
    @Column
    private BigDecimal devolucaoValorRealizado;
    @Column(length = 1000)
    private String devolucaoObservacoes;
    @Column(length = 80)
    private String devolucaoResponsavelColaborador;
    @Column(length = 80)
    private String devolucaoResponsavelFornecedor;

//    @OneToMany
    @OneToMany(
//            mappedBy = "post",
            cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name="OrdemServicoId")
    private List<OrdemServicoOcorrencia> ocorrencias = new ArrayList<OrdemServicoOcorrencia>();

    public OrdemServico() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getAbertura() {
        return abertura;
    }

    public void setAbertura(LocalDate abertura) {
        this.abertura = abertura;
    }

    public NaturezaServico getNaturezaServico() {
        return naturezaServico;
    }

    public void setNaturezaServico(NaturezaServico naturezaServico) {
        this.naturezaServico = naturezaServico;
    }

//    @JsonIgnore
    public Veiculo getVeiculo() {
        return veiculo;
    }

//    @JsonIgnore
    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Long getVeiculoId() {
        return veiculo == null ? null : veiculo.getId();
    }

    public void setVeiculoId(Long id) {
        VeiculoService vs = new VeiculoServiceImpl();
        this.veiculo = vs.findById(id).get();
    }

    public LocalDateTime getDevolucaoPrevisao() {
        return devolucaoPrevisao;
    }

    public void setDevolucaoPrevisao(LocalDateTime devolucaoPrevisao) {
        this.devolucaoPrevisao = devolucaoPrevisao;
    }

    public String getNumeroOsFornecedor() {
        return numeroOsFornecedor;
    }

    public void setNumeroOsFornecedor(String numeroOsFornecedor) {
        this.numeroOsFornecedor = numeroOsFornecedor;
    }

    public SituacaoServico getSituacaoServico() {
        return situacaoServico;
    }

    public void setSituacaoServico(SituacaoServico situacaoServico) {
        this.situacaoServico = situacaoServico;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public LocalDateTime getRetiradaDataHora() {
        return retiradaDataHora;
    }

    public void setRetiradaDataHora(LocalDateTime retiradaDataHora) {
        this.retiradaDataHora = retiradaDataHora;
    }

    public BigDecimal getRetiradaValorPrevisto() {
        return retiradaValorPrevisto;
    }

    public void setRetiradaValorPrevisto(BigDecimal retiradaValorPrevisto) {
        this.retiradaValorPrevisto = retiradaValorPrevisto;
    }

    public String getRetiradaObservacoes() {
        return retiradaObservacoes;
    }

    public void setRetiradaObservacoes(String retiradaObservacoes) {
        this.retiradaObservacoes = retiradaObservacoes;
    }

    public String getRetiradaResponsavelColaborador() {
        return retiradaResponsavelColaborador;
    }

    public void setRetiradaResponsavelColaborador(String retiradaResponsavelColaborador) {
        this.retiradaResponsavelColaborador = retiradaResponsavelColaborador;
    }

    public String getRetiradaResponsavelFornecedor() {
        return retiradaResponsavelFornecedor;
    }

    public void setRetiradaResponsavelFornecedor(String retiradaResponsavelFornecedor) {
        this.retiradaResponsavelFornecedor = retiradaResponsavelFornecedor;
    }

    public LocalDateTime getDevolucaoDataHora() {
        return devolucaoDataHora;
    }

    public void setDevolucaoDataHora(LocalDateTime devolucaoDataHora) {
        this.devolucaoDataHora = devolucaoDataHora;
    }

    public BigDecimal getDevolucaoValorRealizado() {
        return devolucaoValorRealizado;
    }

    public void setDevolucaoValorRealizado(BigDecimal devolucaoValorRealizado) {
        this.devolucaoValorRealizado = devolucaoValorRealizado;
    }

    public String getDevolucaoObservacoes() {
        return devolucaoObservacoes;
    }

    public void setDevolucaoObservacoes(String devolucaoObservacoes) {
        this.devolucaoObservacoes = devolucaoObservacoes;
    }

    public String getDevolucaoResponsavelColaborador() {
        return devolucaoResponsavelColaborador;
    }

    public void setDevolucaoResponsavelColaborador(String devolucaoResponsavelColaborador) {
        this.devolucaoResponsavelColaborador = devolucaoResponsavelColaborador;
    }

    public String getDevolucaoResponsavelFornecedor() {
        return devolucaoResponsavelFornecedor;
    }

    public void setDevolucaoResponsavelFornecedor(String devolucaoResponsavelFornecedor) {
        this.devolucaoResponsavelFornecedor = devolucaoResponsavelFornecedor;
    }

    public List<OrdemServicoOcorrencia> getOcorrencias() {
        return ocorrencias;
    }

    public void setOcorrencias(List<OrdemServicoOcorrencia> ocorrencias) {
        this.ocorrencias = ocorrencias;
        for (OrdemServicoOcorrencia ocorrencia : this.ocorrencias) {
            if (! ocorrencia.getOrdemServico().equals(this)) {
                ocorrencia.setOrdemServico(this);
            }
        }
    }

    public void addOrdemServicoOcorrencia(OrdemServicoOcorrencia ocorrencia) {
        if (ocorrencia != null) {
            this.ocorrencias.add(ocorrencia);
            ocorrencia.setOrdemServico(this);
        }
    }

    public void removeOrdemServicoOcorrencia(OrdemServicoOcorrencia ocorrencia) {
        if (ocorrencia != null) {
            this.ocorrencias.remove(ocorrencia);
        }
    }

}

package br.net.gradual.business.model;

import br.net.gradual.business.model.collections.Genero;
import br.net.gradual.business.model.collections.TipoJuridico;
import br.net.gradual.core.model.AbstractEntity;
import br.net.gradual.core.model.Estado;
import br.net.gradual.core.model.Municipio;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Fornecedor",
        indexes = {@Index(name = "idx_cpf_cnpj", columnList = "cpfCnpj") } ,
        uniqueConstraints={@UniqueConstraint(name="UK_Fornecedor_CpfCnpj", columnNames="cpfCnpj" )} )
public class Fornecedor extends AbstractEntity {

    @NotNull
    private TipoJuridico tipoJuridico;

    @NotEmpty
    @Column(length = 20)
    private String cpfCnpj;

    @NotEmpty
    @Column(length = 200)
    private String nomeRazaoSocial;

    @Column(length = 200)
    private String apelidoNomeFantasia;

    @NotNull
    @Column(columnDefinition = "CHAR(3)")
    private Genero genero;

    @Column(length = 20)
    private String identidadeInscricaoEstadual;

    @Column
    private LocalDate nascimentoAbertura;

    @Email
    @Column(length = 100)
    private String email;

    @NotNull
    @Column(columnDefinition = "boolean DEFAULT 1")
    private Boolean ativo;

    @Column(columnDefinition = "TEXT(2000)", length = 2000)
    private String notas;

    @Column(length = 10)
    private String cep;

    @NotEmpty
    @Column(length = 100)
    private String logradouro;

    @Column(length = 100)
    private String complemento;

    @Column(length = 80)
    private String bairro;

    @ManyToOne
    @JoinColumn(name = "uf", referencedColumnName = "uf", foreignKey = @ForeignKey(name = "FK_Fornecedor_Estado"))
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "municipioId", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Fornecedor_Municipio"))
    private Municipio municipio;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable( name = "Fornecedor_Telefone",
            joinColumns = { @JoinColumn(name = "fornecedor_id", foreignKey=@ForeignKey(name = "FK_Fornecedor_Telefone_Fornecedor")) },
            inverseJoinColumns = { @JoinColumn(name = "telefone_id", foreignKey=@ForeignKey(name = "FK_Fornecedor_Telefone_Telefone"))} )
    private List<Telefone> telefones = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.REMOVE)
    @JoinTable( name = "Fornecedor_Atividade_Economica",
            joinColumns = { @JoinColumn(name = "fornecedor_id", foreignKey=@ForeignKey(name = "FK_Fornecedor_Atividade_Economica_Fornecedor")) },
            inverseJoinColumns = { @JoinColumn(name = "atividade_economica_id", foreignKey=@ForeignKey(name = "FK_Fornecedor_Atividade_Economica_Atividade_Economica"))} )
    private List<AtividadeEconomica> atividadesEconomicas = new ArrayList<>();

    public Fornecedor() {
    }

    public Fornecedor(@NotNull TipoJuridico tipoJuridico, @NotEmpty String cpfCnpj, @NotEmpty String nomeRazaoSocial) {
        this.tipoJuridico = tipoJuridico;
        this.cpfCnpj = cpfCnpj;
        this.nomeRazaoSocial = nomeRazaoSocial;
    }

    public TipoJuridico getTipoJuridico() {
        return tipoJuridico;
    }

    public void setTipoJuridico(TipoJuridico tipoJuridico) {
        this.tipoJuridico = tipoJuridico;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getNomeRazaoSocial() {
        return nomeRazaoSocial;
    }

    public void setNomeRazaoSocial(String nomeRazaoSocial) {
        this.nomeRazaoSocial = nomeRazaoSocial;
    }

    public String getApelidoNomeFantasia() {
        return apelidoNomeFantasia;
    }

    public void setApelidoNomeFantasia(String apelidoNomeFantasia) {
        this.apelidoNomeFantasia = apelidoNomeFantasia;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public String getIdentidadeInscricaoEstadual() {
        return identidadeInscricaoEstadual;
    }

    public void setIdentidadeInscricaoEstadual(String identidadeInscricaoEstadual) {
        this.identidadeInscricaoEstadual = identidadeInscricaoEstadual;
    }

    public LocalDate getNascimentoAbertura() {
        return nascimentoAbertura;
    }

    public void setNascimentoAbertura(LocalDate nascimentoAbertura) {
        this.nascimentoAbertura = nascimentoAbertura;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public List<AtividadeEconomica> getAtividadesEconomicas() {
        return atividadesEconomicas;
    }

    public void setAtividadesEconomicas(List<AtividadeEconomica> atividadesEconomicas) {
        this.atividadesEconomicas = atividadesEconomicas;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Override
    public String toString() {
        return "Fornecedor{" +
                "tipoJuridico=" + tipoJuridico +
                ", cpfCnpj='" + cpfCnpj + '\'' +
                ", nomeRazaoSocial='" + nomeRazaoSocial + '\'' +
                ", apelidoNomeFantasia='" + apelidoNomeFantasia + '\'' +
                ", genero=" + genero +
                ", identidadeInscricaoEstadual='" + identidadeInscricaoEstadual + '\'' +
                ", nascimentoAbertura=" + nascimentoAbertura +
                ", email='" + email + '\'' +
                ", ativo=" + ativo +
                ", notas='" + notas + '\'' +
                ", cep='" + cep + '\'' +
                ", logradouro='" + logradouro + '\'' +
                ", complemento='" + complemento + '\'' +
                ", bairro='" + bairro + '\'' +
                ", estado=" + estado +
                ", municipio=" + municipio +
                ", telefones=" + telefones +
                ", atividadesEconomicas=" + atividadesEconomicas +
                '}';
    }
}

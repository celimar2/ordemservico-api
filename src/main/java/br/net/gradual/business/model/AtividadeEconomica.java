package br.net.gradual.business.model;

import br.net.gradual.core.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "AtividadeEconomica",
        uniqueConstraints=@UniqueConstraint(name="UK_AtividadeEconomica_nome", columnNames="nome"))
public class AtividadeEconomica extends AbstractEntity {

    @NotEmpty
    @Column(length = 100)
    private String nome;

    @NotNull
    @Column(columnDefinition = "boolean DEFAULT 1")
    private Boolean ativo;

    public AtividadeEconomica() {
    }

    public AtividadeEconomica(@NotEmpty String nome, @NotNull Boolean ativo) {
        this.nome = nome;
        this.ativo = ativo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AtividadeEconomica that = (AtividadeEconomica) o;
        return Objects.equals(nome, that.nome);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), nome);
    }
}
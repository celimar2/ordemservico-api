package br.net.gradual.business.model.collections;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SituacaoAtivoInativoConverter implements AttributeConverter<SituacaoAtivoInativo , String> {

    @Override
    public String convertToDatabaseColumn(SituacaoAtivoInativo  genero) {
        return genero.getValor();
    }

    @Override
    public SituacaoAtivoInativo  convertToEntityAttribute(String dbData) {
        return SituacaoAtivoInativo.fromValor(dbData);
    }

}
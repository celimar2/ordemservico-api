package br.net.gradual.business.model.collections;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum Genero implements Serializable {

    FEMINIMO ("FEM"),
    MASCULINO ("MAS"),
    OUTRO("OUT");

    private final String valor;

    Genero(final String valor) {
        this.valor = valor;
    }

    @JsonValue
    public String getValor() {
        return this.valor;
    }

    public static Genero fromValor(String valor) {
        for (Genero gen : Genero.values()) {
            if (gen.getValor().equals(valor)) {
                return gen;
            }
        }
        throw new IllegalArgumentException("Valor [" + valor+ "] não suportado.");
    }

}
package br.net.gradual.business.model.collections;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum SituacaoVeiculo implements Serializable {

    AVOLUE_PENDENTE     ("1-AVOPEN"),
    AGUARDANDO_RESPOSTA ("2-AGURES"),
    AVOLUE_APROVADO     ("3-AVOAPR"),
    EM_PREPARACAO       ("4-EMPREP"),
    PREPARACAO_OK       ("5-PREPOK"),
    ATACADO             ("6-ATACADO");

    private final String valor;

    SituacaoVeiculo(String valor) {
        this.valor = valor;
    }

    @JsonValue
    public String getValor(){
        return valor;
    }

    public static SituacaoVeiculo fromValor(String valor) {
        for (SituacaoVeiculo tt : SituacaoVeiculo.values()) {
            if (tt.getValor().equals(valor))
                return tt;
        }
        throw new IllegalArgumentException("Valor [" + valor+ "] não suportado.");
    }



}

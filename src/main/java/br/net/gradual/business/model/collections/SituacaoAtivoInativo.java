package br.net.gradual.business.model.collections;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum SituacaoAtivoInativo implements Serializable {

    ATIVO ("AT"),
    INATIVO ("IN");

    private final String valor;

    SituacaoAtivoInativo(String valor) {
        this.valor = valor;
    }

    @JsonValue
    public String getValor(){
        return valor;
    }

    public static SituacaoAtivoInativo fromValor(String valor) {
        for (SituacaoAtivoInativo ai : SituacaoAtivoInativo.values()) {
            if (ai.getValor().equals(valor)) {
                return ai;
            }
        }
        throw new IllegalArgumentException("Valor [" + valor+ "] não suportado.");
    }

}

package br.net.gradual.business.model;


import br.net.gradual.business.model.collections.Genero;
import br.net.gradual.core.model.AbstractEntity;
import br.net.gradual.core.model.Estado;
import br.net.gradual.core.model.Municipio;
import br.net.gradual.core.model.User;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "Colaborador",
        indexes = {@Index(name = "idx_nome", columnList = "nome"),
                    @Index(name = "idx_cpf", columnList = "cpf")} ,
        uniqueConstraints = {
            @UniqueConstraint( columnNames = {"apelido"}, name="UK_Colaborador_Apelido"),
            @UniqueConstraint( columnNames = {"cpf"}, name="UK_Colaborador_Cpf") })
public class Colaborador extends AbstractEntity {

    @Column(length = 150)
    private String nome;

    @Column(name = "apelido", length = 50)
    private String apelido;

    @Column(name = "cpf", length = 14)
    private String cpf;

    @Column(length = 15)
    private String identidadeNumero;

    @Column(length = 10)
    private String identidadeOrgaoExpedidor;

    @Column(length = 10)
    private Genero genero;

    @Column
    private LocalDate nascimento;

    @Column(length = 100)
    @Email
    private String email;

    @Column(columnDefinition = "Boolean DEFAULT 1")
    private Boolean ativo;

    @Column(columnDefinition = "TEXT(2000)", length = 2000)
    private String notas;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinTable( name = "Colaborador_User",
            uniqueConstraints=@UniqueConstraint(name="UK_User", columnNames={"user_id"}),
            joinColumns = { @JoinColumn(name = "colaborador_id", foreignKey=@ForeignKey(name = "FK_Colaborador_User_Colaborador")) },
            inverseJoinColumns = { @JoinColumn(name = "user_id", foreignKey=@ForeignKey(name = "FK_Colaborador_User_User"))} )
    private User user;

    @Column(length = 10)
    private String cep;

    @NotEmpty
    @Column(length = 100)
    private String logradouro;

    @Column(length = 100)
    private String complemento;

    @Column(length = 80)
    private String bairro;

    @ManyToOne
    @JoinColumn(name = "uf", referencedColumnName = "uf", foreignKey = @ForeignKey(name = "FK_Colaborador_Estado"))
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "municipioId", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Colaborador_Municipio"))
    private Municipio municipio;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable( name = "Colaborador_Telefone",
            joinColumns = { @JoinColumn(name = "colaborador_id", foreignKey=@ForeignKey(name = "FK_TT_Fornecedor")) },
            inverseJoinColumns = { @JoinColumn(name = "telefone_id", foreignKey=@ForeignKey(name = "FK_FT_Telefone"))} )
    private List<Telefone> telefones = new ArrayList<>();


    @ManyToMany(cascade = { CascadeType.REMOVE })
    @JoinTable( name = "Colaborador_Filial", foreignKey=@ForeignKey(name = "FK_Colaborador_Filial"),
            joinColumns = { @JoinColumn(name = "colaborador_id") },
            inverseJoinColumns = { @JoinColumn(name = "filial_id") } )
    private Set<Filial> filial = new HashSet<>();

    public Colaborador() {
    }

    public Colaborador(@NotEmpty String nome, @NotEmpty String apelido, @NotEmpty String cpf) {
        this.nome = nome;
        this.apelido = apelido;
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getIdentidadeNumero() {
        return identidadeNumero;
    }

    public void setIdentidadeNumero(String identidadeNumero) {
        this.identidadeNumero = identidadeNumero;
    }

    public String getIdentidadeOrgaoExpedidor() {
        return identidadeOrgaoExpedidor;
    }

    public void setIdentidadeOrgaoExpedidor(String identidadeOrgaoExpedidor) {
        this.identidadeOrgaoExpedidor = identidadeOrgaoExpedidor;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public User getUser() {
        return user ;
    }

    public void setUser(User user ) {
        this.user  = user ;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public Set<Filial> getFilial() {
        return filial;
    }

    public void setFilial(Set<Filial> filial) {
        this.filial = filial;
    }

}

package br.net.gradual.business.model.collections;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TipoJuridicoConverter implements AttributeConverter<TipoJuridico , String> {

    @Override
    public String convertToDatabaseColumn(TipoJuridico  genero) {
        return genero.getValor();
    }

    @Override
    public TipoJuridico  convertToEntityAttribute(String dbData) {
        return TipoJuridico .fromValor(dbData);
    }

}
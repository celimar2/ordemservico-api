package br.net.gradual.business.model;

import br.net.gradual.core.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

@Entity
@Table(name = "NaturezaServico",
        uniqueConstraints={@UniqueConstraint(name="UK_NaturezaServico_Nome", columnNames="nome")})
public class NaturezaServico extends AbstractEntity {

    @NotEmpty
    @Column(length = 100)
    private String nome;

    @Column(length = 1000)
    private String observacao;

    @Column(columnDefinition = "Boolean DEFAULT 1")
    private boolean ativo;

    public NaturezaServico() {
    }

    public NaturezaServico(@NotEmpty String nome) {
        this.setNome(nome);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase();
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NaturezaServico that = (NaturezaServico) o;
        return Objects.equals(nome, that.nome);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), nome);
    }
}

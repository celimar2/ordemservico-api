package br.net.gradual.business.model.collections;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum TipoTelefone implements Serializable {

    CELULAR ("CEL"),
    RESIDENCIAL ("RES"),
    COMERCIAL ("COM"),
    RECADO ("REC");

    private final String valor;

    TipoTelefone(String valor) {
        this.valor = valor;
    }

    @JsonValue
    public String getValor(){
        return valor;
    }

    public static TipoTelefone fromValor(String valor) {
        for (TipoTelefone tt : TipoTelefone.values()) {
            if (tt.getValor().equals(valor))
                return tt;
        }
        throw new IllegalArgumentException("Valor [" + valor+ "] não suportado.");
    }

}

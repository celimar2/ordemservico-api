package br.net.gradual.business.repository;

import br.net.gradual.business.model.Veiculo;
import br.net.gradual.business.model.VeiculoSituacaoHistorico;
import br.net.gradual.business.model.collections.SituacaoVeiculo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface VeiculoSituacaoHistoricoRepository extends CrudRepository<VeiculoSituacaoHistorico, Long> {
    List<VeiculoSituacaoHistorico> findAllByVeiculo(Veiculo veiculo);
    List<VeiculoSituacaoHistorico> findAllByDataBetween(
            LocalDateTime dataIni,
            LocalDateTime  dataFim);
    List<VeiculoSituacaoHistorico> findAllByDataBetweenAndSituacaoIsIn(
            LocalDateTime dataIni,
            LocalDateTime dataFim,
            List<SituacaoVeiculo> situacaoVeiculos);

}

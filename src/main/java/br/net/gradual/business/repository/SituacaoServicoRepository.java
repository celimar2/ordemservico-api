package br.net.gradual.business.repository;

import br.net.gradual.business.model.SituacaoServico;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SituacaoServicoRepository extends PagingAndSortingRepository<SituacaoServico, Long> {
}

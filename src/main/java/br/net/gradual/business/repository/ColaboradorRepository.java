package br.net.gradual.business.repository;

import br.net.gradual.business.model.Colaborador;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ColaboradorRepository extends PagingAndSortingRepository<Colaborador, Long> {
    Colaborador findByCpf(String cpf);
}

package br.net.gradual.business.repository;

import br.net.gradual.business.model.Fornecedor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FornecedorRepository extends PagingAndSortingRepository<Fornecedor, Long> {
    Fornecedor findByCpfCnpj(String cpfCnpj);
}

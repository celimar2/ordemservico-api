package br.net.gradual.business.repository;

import br.net.gradual.business.model.Filial;
import org.springframework.data.repository.CrudRepository;

public interface FilialRepository extends CrudRepository<Filial, Long> {
}

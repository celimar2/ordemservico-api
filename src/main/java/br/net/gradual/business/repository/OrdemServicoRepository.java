package br.net.gradual.business.repository;

import br.net.gradual.business.model.OrdemServico;
import br.net.gradual.business.model.Veiculo;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


public interface OrdemServicoRepository extends PagingAndSortingRepository<OrdemServico, Long> {
    List<OrdemServico> findAllByVeiculo (Veiculo veiculo);
}

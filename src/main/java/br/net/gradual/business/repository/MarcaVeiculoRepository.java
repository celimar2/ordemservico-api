package br.net.gradual.business.repository;

import br.net.gradual.business.model.MarcaVeiculo;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface MarcaVeiculoRepository extends PagingAndSortingRepository<MarcaVeiculo, Long> {
}

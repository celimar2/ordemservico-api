package br.net.gradual.business.repository;

import br.net.gradual.business.model.Veiculo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface VeiculoRepository extends CrudRepository<Veiculo, Long> {
    Optional<Veiculo> findById(Long id);
    Veiculo findByPlaca(String placa);
    List<Veiculo> findAllByOrderByPlaca();
    void deleteById(Long id);
}

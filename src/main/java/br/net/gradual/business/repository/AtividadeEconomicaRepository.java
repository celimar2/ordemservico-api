package br.net.gradual.business.repository;

import br.net.gradual.business.model.AtividadeEconomica;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AtividadeEconomicaRepository extends PagingAndSortingRepository<AtividadeEconomica, Long> {
    List<AtividadeEconomica> findByAtivo(Boolean ativo);

}

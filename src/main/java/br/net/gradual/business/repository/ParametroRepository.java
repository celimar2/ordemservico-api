package br.net.gradual.business.repository;

import br.net.gradual.business.model.Parametro;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ParametroRepository extends PagingAndSortingRepository<Parametro, String> {
    List<Parametro> findByChaveContaining(String chave);
}

package br.net.gradual.business.repository;

import br.net.gradual.business.model.OrdemServicoOcorrencia;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrdemServicoOcorrenciaRepository extends PagingAndSortingRepository<OrdemServicoOcorrencia, Long> {
}

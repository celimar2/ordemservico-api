package br.net.gradual.business.repository;

import br.net.gradual.business.model.NaturezaServico;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface NaturezaServicoRepository extends PagingAndSortingRepository<NaturezaServico, Long> {
}

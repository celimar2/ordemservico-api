package br.net.gradual.business.service;

import br.net.gradual.business.model.Veiculo;

import java.util.List;
import java.util.Optional;

public interface VeiculoService {

    List<Veiculo> findAll();

    Veiculo save(Veiculo veiculo);

    void delete(long id);

    void deleteById(long id);

    Veiculo findByPlaca(String placa);

    Optional<Veiculo> findById(Long id);

}


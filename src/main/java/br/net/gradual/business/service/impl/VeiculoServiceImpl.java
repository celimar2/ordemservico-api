package br.net.gradual.business.service.impl;

import br.net.gradual.business.model.Veiculo;
import br.net.gradual.business.model.VeiculoSituacaoHistorico;
import br.net.gradual.business.model.collections.SituacaoVeiculo;
import br.net.gradual.business.repository.VeiculoRepository;
import br.net.gradual.business.repository.VeiculoSituacaoHistoricoRepository;
import br.net.gradual.business.service.VeiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value = "veiculoService")
public class VeiculoServiceImpl implements VeiculoService {

    @Autowired
    private VeiculoRepository veiculoRepository;

    @Autowired
    private VeiculoSituacaoHistoricoRepository veiculoSituacaoRepository;

//    @Autowired
//    public VeiculoServiceImpl(VeiculoRepository veiculoRepository) {
//        this.veiculoRepository = veiculoRepository;
//    }


    @Override
    public Optional<Veiculo> findById(Long id) {
        return veiculoRepository.findById(id);
    }

    @Override
    public Veiculo save(Veiculo veiculo) {
        Veiculo v3 = null;
        if ((veiculo.getId() != 0) && (veiculo.getId() != null)) {
            String oldSituacao = veiculoRepository.findById(veiculo.getId()).get().getSituacao().getValor();
            v3 = veiculoRepository.save(veiculo);
            String newSituacao = v3.getSituacao().getValor();
            if (!newSituacao.equals(oldSituacao)) {
                VeiculoSituacaoHistorico vs = new VeiculoSituacaoHistorico(veiculo, LocalDateTime.now(), SituacaoVeiculo.fromValor(oldSituacao));
                veiculoSituacaoRepository.save(vs);
            }
        } else {
            v3 = veiculoRepository.save(veiculo);
        }
        return v3;
    }

    @Override
    public List<Veiculo> findAll() {
        List<Veiculo> list = new ArrayList<>();
        veiculoRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        veiculoRepository.deleteById(id);
    }

    @Override
    public void deleteById(long id) {
        veiculoRepository.deleteById(id);
    }

    @Override
    public Veiculo findByPlaca(String placa) {
        return veiculoRepository.findByPlaca(placa);
    }

}
